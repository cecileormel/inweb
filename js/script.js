(function () {
  // get all skip links
  var skipLinks = document.querySelectorAll('[class^="skiplinks"] > *');

  skipLinks.forEach(function (child) {
    // console.log(child.getAttribute('href'));

    var launchChatbox = function (event) {
      // console.log(this, event);
      document.getElementById('chatAbcdeButton').click();
    }

    // if there is a "chatAbcdeContainer" skip link
    if (child.getAttribute('href') === "#chatAbcdeContainer") {
      child.addEventListener('click', launchChatbox);
      child.addEventListener('enter', launchChatbox);
    }
  });
})();

