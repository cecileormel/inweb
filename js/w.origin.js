(function (navigator) {
  navigator.getUserMedia = navigator.getUserMedia || navigator.webkitGetUserMedia || navigator.mozGetUserMedia || navigator.msGetUserMedia;
  var hasGetUserMedia = function () {
    return typeof navigator.getUserMedia != "undefined"
  };
  navigator.hasGetUserMedia = hasGetUserMedia
})(navigator);
(function (window) {
  window.AudioContext = window.AudioContext || window.webkitAudioContext || window.mozAudioContext || window.msAudioContext;
  var hasWebAudioAPI = function () {
    return typeof window.AudioContext != "undefined"
  };
  window.hasWebAudioAPI = hasWebAudioAPI
})(window);
(function (window) {
  window.URL = window.URL || window.webkitURL || window.mozURL || window.msURL || null;
  var hasUrlAPI = function () {
    return typeof window.URL != "undefined"
  };
  window.hasUrlAPI = hasUrlAPI
})(window);
(function (document) {
  document.fullScreenEnabled = document.fullscreenEnabled || document.webkitFullscreenEnabled || document.mozFullScreenEnabled || document.msFullscreenEnabled;
  document.documentElement.requestFullScreen = document.documentElement.requestFullscreen || document.documentElement.webkitRequestFullscreen || document.documentElement.mozRequestFullScreen || document.documentElement.msRequestFullscreen;
  document.exitFullScreen = document.exitFullscreen || document.mozCancelFullScreen || document.webkitExitFullscreen
})(document);
(function (navigator) {
  navigator.vibrate = navigator.vibrate || navigator.webkitVibrate || navigator.mozVibrate || navigator.msVibrate || null
})(navigator);

function isOdd(value) {
  return (value & 1) == 1
}

function isNumber(n) {
  return !isNaN(parseFloat(n)) && isFinite(n)
}

function isDigit(string) {
  return string.replace(/[ .+]/g, "") - parseFloat(string.replace(/[ .+]/g, "")) + 1 >= 0
}

function is_array(input) {
  return typeof input == "object" && input instanceof Array
}

var volume_lang_strings = new Array("fr", "en");
volume_lang_strings["fr"] = new Array("short", "long");
volume_lang_strings["fr"]["none"] = new Array;
volume_lang_strings["fr"]["short"] = new Array;
volume_lang_strings["fr"]["long"] = new Array;
volume_lang_strings["en"] = new Array("short", "long");
volume_lang_strings["en"]["none"] = new Array;
volume_lang_strings["en"]["short"] = new Array;
volume_lang_strings["en"]["long"] = new Array;
volume_lang_strings["fr"]["none"]["o"] = "";
volume_lang_strings["fr"]["none"]["ko"] = "";
volume_lang_strings["fr"]["none"]["mo"] = "";
volume_lang_strings["fr"]["none"]["go"] = "";
volume_lang_strings["fr"]["none"]["to"] = "";
volume_lang_strings["fr"]["none"]["po"] = "";
volume_lang_strings["fr"]["none"]["eo"] = "";
volume_lang_strings["fr"]["none"]["zo"] = "";
volume_lang_strings["fr"]["none"]["yo"] = "";
volume_lang_strings["fr"]["short"]["o"] = " o";
volume_lang_strings["fr"]["short"]["ko"] = " Ko";
volume_lang_strings["fr"]["short"]["mo"] = " Mo";
volume_lang_strings["fr"]["short"]["go"] = " Go";
volume_lang_strings["fr"]["short"]["to"] = " To";
volume_lang_strings["fr"]["short"]["po"] = " Po";
volume_lang_strings["fr"]["short"]["eo"] = " Eo";
volume_lang_strings["fr"]["short"]["zo"] = " Zo";
volume_lang_strings["fr"]["short"]["yo"] = " Yo";
volume_lang_strings["fr"]["long"]["o"] = " Octet(s)";
volume_lang_strings["fr"]["long"]["ko"] = " Kilo-octet(s)";
volume_lang_strings["fr"]["long"]["mo"] = " Mega-octet(s)";
volume_lang_strings["fr"]["long"]["go"] = " Giga-octet(s)";
volume_lang_strings["fr"]["long"]["to"] = " Tera-octet(s)";
volume_lang_strings["fr"]["long"]["po"] = " PÃ©ta_octet(s)";
volume_lang_strings["fr"]["long"]["eo"] = " Exa-octet(s)";
volume_lang_strings["fr"]["long"]["zo"] = " Zetta-octet(s)";
volume_lang_strings["fr"]["long"]["yo"] = " Yotta-octet(s)";
volume_lang_strings["en"]["none"]["o"] = "";
volume_lang_strings["en"]["none"]["ko"] = "";
volume_lang_strings["en"]["none"]["mo"] = "";
volume_lang_strings["en"]["none"]["go"] = "";
volume_lang_strings["en"]["none"]["to"] = "";
volume_lang_strings["en"]["none"]["po"] = "";
volume_lang_strings["en"]["none"]["eo"] = "";
volume_lang_strings["en"]["none"]["zo"] = "";
volume_lang_strings["en"]["none"]["yo"] = "";
volume_lang_strings["en"]["short"]["o"] = " b";
volume_lang_strings["en"]["short"]["ko"] = " Kb";
volume_lang_strings["en"]["short"]["mo"] = " Mb";
volume_lang_strings["en"]["short"]["go"] = " Gb";
volume_lang_strings["en"]["short"]["to"] = " Tb";
volume_lang_strings["en"]["short"]["po"] = " Pb";
volume_lang_strings["en"]["short"]["eo"] = " Eb";
volume_lang_strings["en"]["short"]["zo"] = " Zb";
volume_lang_strings["en"]["short"]["yo"] = " Yb";
volume_lang_strings["en"]["long"]["o"] = " Byte(s)";
volume_lang_strings["en"]["long"]["ko"] = " Kilo-byte(s)";
volume_lang_strings["en"]["long"]["mo"] = " Mega-byte(s)";
volume_lang_strings["en"]["long"]["go"] = " Giga-byte(s)";
volume_lang_strings["en"]["long"]["to"] = " Tera-byte(s)";
volume_lang_strings["en"]["long"]["po"] = " PÃ©ta_byte(s)";
volume_lang_strings["en"]["long"]["eo"] = " Exa-byte(s)";
volume_lang_strings["en"]["long"]["zo"] = " Zetta-byte(s)";
volume_lang_strings["en"]["long"]["yo"] = " Yotta-byte(s)";

function relativeVolume(volume, lang, format) {
  var result = "";
  if (parseFloat(volume) > 0) result = parseFloat(volume) + volume_lang_strings[lang][format]["o"];
  if (parseFloat(volume) > 1024) result = Math.round(parseFloat(volume) / 1024) + volume_lang_strings[lang][format]["ko"];
  if (parseFloat(volume) > 1048576) result = Math.round(parseFloat(volume) / 1048576) + volume_lang_strings[lang][format]["mo"];
  if (parseFloat(volume) > 1073741824) result = Math.round(parseFloat(volume) / 1073741824) + volume_lang_strings[lang][format]["go"];
  if (parseFloat(volume) > 1099511627776) result = Math.round(parseFloat(volume) / 1099511627776) + volume_lang_strings[lang][format]["to"];
  if (parseFloat(volume) > 0x4000000000000) result = Math.round(parseFloat(volume) / 0x4000000000000) + volume_lang_strings[lang][format]["po"];
  if (parseFloat(volume) > 0x1000000000000000) result = Math.round(parseFloat(volume) / 0x1000000000000000) + volume_lang_strings[lang][format]["eo"];
  if (parseFloat(volume) > 11805916207174113e5) result = Math.round(parseFloat(volume) / 11805916207174113e5) + volume_lang_strings[lang][format]["zo"];
  if (parseFloat(volume) > 12089258196146292e8) result = Math.round(parseFloat(volume) / 12089258196146292e8) + volume_lang_strings[lang][format]["yo"];
  return result
}

function randomString(length, chars) {
  chars = chars || "0123456789ABCDEFGHIJKLMNOPQRSTUVWXTZabcdefghiklmnopqrstuvwxyz";
  var string = "", i = 0, rnum = 0;
  for (i = 0; i < length; i++) {
    rnum = Math.floor(Math.random() * chars.length);
    string += chars.substring(rnum, rnum + 1)
  }
  return string
}

Object.size = function (obj) {
  var size = 0, key;
  for (key in obj) {
    if (obj.hasOwnProperty(key)) size++
  }
  return size
};

function classThatManageContextInCallback(object) {
  this._classname = undefined;
  this._lastfunct = undefined;
  this._lastfunctname = undefined;
  this._debounceTimers = [];
  if (object && typeof object == "object") {
    for (var property in object) {
      this[property] = object[property]
    }
  }
  this.bind = function (method) {
    var self = this;
    return function () {
      return method.apply(self, arguments)
    }
  };
  this.debounce = function (callback, time) {
    time = time || 500;
    var classname = this._class(), callbackId = MD5(classname + callback.toString() + time);
    if (typeof callback == "function") {
      if (this._debounceTimers[callbackId]) {
        clearTimeout(this._debounceTimers[callbackId]);
        this._debounceTimers[callbackId] = null
      }
      this._debounceTimers[callbackId] = setTimeout(function () {
        callback()
      }, time)
    }
  };
  this.destroy = function () {
  };
  this._class = function () {
    if (this._classname === undefined && this && this.constructor && this.constructor.toString) {
      var arr = this.constructor.toString().match(/function\s*(\w+)/);
      if (arr && arr.length == 2) {
        this._classname = arr[1]
      }
    }
    return this._classname
  };
  this._method = function (funct) {
    var arr = null, functname = undefined;
    if (this._classname === undefined && this && this.constructor && this.constructor.toString) {
      arr = this.constructor.toString().match(/function\s*(\w+)/);
      if (arr && arr.length == 2) {
        this._classname = arr[1]
      }
    }
    arr = funct.toString().match(/function\s*([A-Za-z0-9_:]+)/);
    functname = undefined;
    if (arr && arr.length == 2) {
      functname = arr[1]
    }
    return this._classname + "::" + functname
  };
  this._properties = function (type, recursive, object) {
    if (recursive === undefined) recursive = true;
    object = object || this;
    var copy = {}, property = null;
    for (property in object) {
      if (typeof object[property] == "function" || type == "public" && property.substr(0, 1) == "_" || type == "private" && property.substr(0, 1) != "_") {
      } else if (object[property] && typeof object[property] == "object" && recursive) {
        copy[property] = this._properties(type, recursive, object[property])
      } else if (object[property] !== undefined) {
        copy[property] = object[property]
      }
    }
    return copy
  }
}

function dec2hex(x) {
  return ("0" + parseInt(x).toString(16)).slice(-2)
}

function hex2dec(x) {
  return parseInt(x, 16)
}

function rgb2hex(rgb) {
  if (typeof rgb != "undefined") {
    rgb = rgb.match(/^rgb\((\d+),\s*(\d+),\s*(\d+)\)$/);
    return dec2hex(rgb[1]) + dec2hex(rgb[2]) + dec2hex(rgb[3])
  } else {
    return undefined
  }
}

function rgba2hex(rgba) {
  rgba = rgba.toLowerCase();
  if (typeof rgba != "undefined") {
    rgba = rgba.match(/^rgb\((\d+),\s*(\d+),\s*(\d+)\)$/);
    return dec2hex(rgba[1]) + dec2hex(rgba[2]) + dec2hex(rgba[3]) + "ff"
  } else {
    return undefined
  }
}

function hex2rgb(hex, a) {
  if (typeof hex != "string") {
    return undefined
  }
  var rgb_string = "";
  a = a || "";
  if (typeof hex != "undefined") {
    rgb_string = "rgb(";
    rgb_string += hex2dec(hex.substr(0, 2));
    rgb_string += "," + hex2dec(hex.substr(2, 2));
    rgb_string += "," + hex2dec(hex.substr(4, 2));
    rgb_string += ")";
    return rgb_string
  } else {
    return undefined
  }
}

function hex2rgba(hex, a) {
  if (typeof hex != "string") {
    return undefined
  }
  var rgba_string = "";
  a = a || "";
  if (typeof hex != "undefined") {
    rgba_string = "rgba(";
    rgba_string += hex2dec(hex.substr(0, 2));
    rgba_string += "," + hex2dec(hex.substr(2, 2));
    rgba_string += "," + hex2dec(hex.substr(4, 2));
    if (a == "" && hex.length > 6) a = hex2dec(hex.substr(6, 2));
    if (a == "") a = 255;
    rgba_string += "," + a / 255;
    rgba_string += ")";
    return rgba_string
  } else {
    return undefined
  }
}

function stringToHex(str, separator) {
  separator = separator || "";
  var tmp_separator = "", hex = "", charVal = 0, charVal1 = 0, charVal2 = 0, i = 0;
  for (i = 0; i < str.length; i += 1) {
    charVal = str.charCodeAt(i);
    if (charVal > 65535) {
    } else if (charVal > 255) {
      charVal1 = Math.floor(charVal / 256).toString(16);
      if (charVal1.length < 2) {
        charVal1 = "0" + charVal1
      }
      hex += tmp_separator + charVal1;
      tmp_separator = separator;
      charVal2 = (charVal % 256).toString(16);
      if (charVal2.length < 2) {
        charVal2 = "0" + charVal2
      }
      hex += tmp_separator + charVal2
    } else {
      charVal = charVal.toString(16);
      if (charVal.length < 2) charVal = "0" + charVal;
      hex += tmp_separator + charVal;
      tmp_separator = separator
    }
  }
  return hex
}

function hexToString(hex) {
  var str = "", charVal = "", i = 0;
  hex = hex.replace(/ /g, "");
  for (i = 0; i < hex.length / 2; i += 1) {
    charVal = String.fromCharCode(parseInt(hex.substr(i * 2, 2), 16));
    str += charVal
  }
  return str
}

function stringToBytes(str) {
  var ch, st, re = [], i;
  for (i = 0; i < str.length; i++) {
    ch = str.charCodeAt(i);
    st = [];
    do {
      st.push(ch & 255);
      ch = ch >> 8
    } while (ch);
    re = re.concat(st.reverse())
  }
  return re
}

function base64ToBlob(b64Data, contentType, sliceSize) {
  contentType = contentType || "";
  sliceSize = sliceSize || 512;
  var byteCharacters = atob(b64Data), byteArrays = [], slice = null, byteNumbers = null, byteArray = null, offset = 0,
    i = 0;
  for (offset = 0; offset < byteCharacters.length; offset += sliceSize) {
    slice = byteCharacters.slice(offset, offset + sliceSize);
    byteNumbers = new Array(slice.length);
    for (i = 0; i < slice.length; i++) {
      byteNumbers[i] = slice.charCodeAt(i)
    }
    byteArray = new Uint8Array(byteNumbers);
    byteArrays.push(byteArray)
  }
  return new Blob(byteArrays, {type: contentType})
}

function base64ToBlobUrl(b64Data, contentType, sliceSize) {
  return URL.createObjectURL(base64ToBlob(b64Data, contentType, sliceSize))
}

function uuidv4() {
  return ([1e7] + -1e3 + -4e3 + -8e3 + -1e11).replace(/[018]/g, c => (c ^ crypto.getRandomValues(new Uint8Array(1))[0] & 15 >> c / 4).toString(16))
}

function wsajax(request) {
  request.method = request.method || "GET";
  request.type = request.type || "text";
  request.data = request.data || null;
  request.headers = request.headers || new Array;
  request.onerror = request.onerror || null;
  request.onload = request.onload || null;
  request.onprogress = request.onprogress || null;
  request.onsuccess = request.onsuccess || request.onload || null;
  var xhr, header = null;
  xhr = new XMLHttpRequest;
  xhr.open(request.method, request.url, true);
  xhr.responseType = request.type;
  xhr.onerror = request.onerror;
  xhr.onprogress = request.onprogress;
  for (header in request.headers) {
    xhr.setRequestHeader(header, request.headers[header])
  }
  xhr.onload = function (event) {
    if (this.status < 200 || this.status > 299) {
      if (typeof request.onerror == "function") {
        request.onerror({type: "error", httpCode: this.status, httpString: this.statusText, fault: null})
      }
      return false
    }
    if (this.response && typeof this.response.response != "undefined" && typeof this.response.response.fault != "undefined") {
      if (typeof request.onerror == "function") {
        request.onerror({
          type: "error",
          httpCode: this.status,
          httpString: this.statusText,
          fault: this.response.response.fault,
          toString: function () {
            return JSON.stringify(this)
          }
        })
      }
      return false
    }
    if (typeof request.onsuccess == "function") request.onsuccess(this.response);
    return true
  };
  xhr.send(request.data);
  return xhr
}

push.prototype = new classThatManageContextInCallback;

function push(server, log, timeout) {
  this._server = server;
  this._log = log || null;
  this._timeout = timeout || 3e4;
  this._type = "longpolling";
  this._id = null;
  this._device = null;
  this._channel = null;
  this._access_token = null;
  this._receive_timeout = this._timeout / 1e3;
  this._receive_loop = false;
  this._receive_retry_tempo = 15e3;
  this._lastEventTriggered = "";
  this.start = function (id, device, notification_callback, error_callback, timeout) {
    this._id = id || null;
    this._device = device || null;
    this._notification_callback = notification_callback || null;
    this._error_callback = error_callback || null;
    timeout = timeout || this._timeout;
    if (timeout > 0) this._timeout = timeout;
    this._receive_loop = true;
    this.subscribe(this._id, this._device, this.bind(function () {
      this.receive(this._notification_callback, this._error_callback, this._timeout);
      this._trigger("push.connectivityUp")
    }), this._error_callback)
  };
  this.stop = function (timeout) {
    timeout = timeout || this._timeout;
    if (timeout > 0) this._timeout = timeout;
    this._receive_loop = false;
    this.unsubscribe(null, this._error_callback)
  };
  this.subscribe = function (id, device, success_callback, error_callback, timeout) {
    this._id = id || this._id;
    this._device = device || this._device;
    timeout = timeout || this._timeout;
    if (timeout > 0) this._timeout = timeout;
    wsajax({
      method: "POST",
      type: "json",
      url: this._server + "/push/subscribe",
      headers: {"Content-type": "application/json;charset=UTF-8", Authorization: "Bearer " + this._access_token},
      data: JSON.stringify({ws_type: this._type, id: id, device: device}),
      onsuccess: this.bind(function (data, status, jqXhr) {
        this._subscribe_success(data, status, jqXhr, success_callback, error_callback, log)
      }),
      onerror: function (error, status, jqXhr, url) {
        this._subscribe_error(error, status, jqXhr, url, error_callback, log)
      }
    })
  };
  this._subscribe_success = function (data, status, jqXhr, success_callback, error_callback, log) {
    var url = "", error_code = "", error_string = "", error_details = "";
    if (!data) {
      error_code = "501";
      error_string = "Empty response from server";
      error_details = ""
    } else if (typeof data.response == "undefined") {
      error_code = "501";
      error_string = "Malformed response from server. Response level missing.";
      error_details = JSON.stringify(data)
    } else if (typeof data.response.fault != "undefined") {
      error_code = data.response.fault.faultcode;
      error_string = data.response.fault.faultstring;
      error_details = data.response.fault.faultdetails
    } else if (typeof data.response.channel == "undefined") {
      error_code = "501";
      error_string = "Malformed response from server. Channel missing.";
      error_details = JSON.stringify(data)
    } else if (typeof data.response.timeout == "undefined") {
      error_code = "501";
      error_string = "Malformed response from server. Timeout missing.";
      error_details = JSON.stringify(data)
    } else {
      this._channel = data.response.channel;
      this._timeout = data.response.timeout * 1e3;
      this._receive_timeout = data.response.receive_timeout;
      if (log) {
        log.debug("(push::_subscribe_success)", "Success, channel=" + this._channel + ",timeout=" + this._receive_timeout)
      }
      if (success_callback) {
        success_callback(data.response)
      }
      return true
    }
    if (log) {
      log.error("(push::_subscribe_success)", "Error (" + error_code + ")," + error_string)
    }
    if (error_callback) error_callback(error_code, error_string, url, error_details, "subscribe");
    return false
  };
  this._subscribe_error = function (error, status, jqXhr, url, callback, log) {
    if (log) {
      log.error("(push::_subscribe_error)", "Error (" + status + ")," + error)
    }
    if (callback) {
      callback(status, error, url, jqXhr.responseText, "subscribe")
    }
  };
  this.unsubscribe = function (success_callback, error_callback, timeout) {
    timeout = timeout || this._timeout;
    if (timeout > 0) this._timeout = timeout;
    wsajax({
      method: "POST",
      type: "json",
      url: this._server + "/push/unsubscribe",
      headers: {"Content-type": "application/json;charset=UTF-8"},
      data: JSON.stringify({ws_type: this._type, channel: this._channel}),
      onsuccess: this.bind(function (data, status, jqXhr) {
        this._unsubscribe_success(data, status, jqXhr, success_callback, error_callback, log)
      }),
      onerror: function (error, status, jqXhr, url) {
        this._unsubscribe_error(error, status, jqXhr, url, error_callback, log)
      }
    })
  };
  this._unsubscribe_success = function (data, status, jqXhr, success_callback, error_callback, log) {
    var url = "", error_code = "", error_string = "", error_details = "";
    if (!data) {
      error_code = "501";
      error_string = "Empty response from server";
      error_details = ""
    } else if (typeof data.response == "undefined") {
      error_code = "501";
      error_string = "Malformed response from server. Response level missing.";
      error_details = JSON.stringify(data)
    } else if (typeof data.response.fault != "undefined") {
      error_code = data.response.fault.faultcode;
      error_string = data.response.fault.faultstring;
      error_details = data.response.fault.faultdetails
    } else if (typeof data.response.result == "undefined") {
      error_code = "501";
      error_string = "Malformed response from server. Result missing.";
      error_details = JSON.stringify(data)
    } else {
      if (log) {
        log.debug("(push::_unsubscribe_success)", "Success,result=" + data.response.result)
      }
      if (success_callback) {
        success_callback(data.response)
      }
      return true
    }
    if (log) {
      log.error("(push::_unsubscribe_success)", "Error (" + error_code + ")," + error_string)
    }
    if (error_callback) {
      error_callback(error_code, error_string, url, error_details, "unsubscribe")
    }
    return false
  };
  this._unsubscribe_error = function (error, status, jqXhr, url, callback, log) {
    if (log) {
      log.error("(push::_unsubscribe_error)", "Error (" + status + ")," + error)
    }
    if (callback) {
      callback(status, error, url, jqXhr.responseText, "unsubscribe")
    }
  };
  this.receive = function (success_callback, error_callback, timeout) {
    timeout = timeout || this._timeout;
    if (timeout > 0) this._timeout = timeout;
    wsajax({
      method: "POST",
      type: "json",
      url: this._server + "/push/receive",
      headers: {"Content-type": "application/json;charset=UTF-8", Authorization: "Bearer " + this._access_token},
      data: JSON.stringify({
        ws_type: this._type,
        channel: this._channel,
        timeout: this._receive_timeout,
        tag: Math.round(1e4 * Math.random())
      }),
      onsuccess: this.bind(function (data, status, jqXhr) {
        this._receive_success(data, status, jqXhr, success_callback, error_callback, log)
      }),
      onerror: function (error, status, jqXhr, url) {
        this._receive_error(error, status, jqXhr, url, error_callback, log)
      }
    })
  };
  this._receive_success = function (data, status, jqXhr, success_callback, error_callback, log) {
    var url = "", error_code = "", error_string = "", error_details = "";
    if (!data) {
      error_code = "501";
      error_string = "Empty response from server";
      error_details = ""
    } else if (typeof data.response == "undefined") {
      error_code = "501";
      error_string = "Malformed response from server. Response level missing.";
      error_details = JSON.stringify(data)
    } else if (typeof data.response.fault != "undefined") {
      switch (data.response.fault.faultcode) {
        case"000":
          this.subscribe(this._id, this._device, this.bind(function () {
            this.receive(success_callback, error_callback, this._timeout)
          }), this._error_callback);
          break;
        case"001":
          this.unsubscribe(this.bind(function () {
            this.subscribe(this._id, this._device, this.bind(function () {
              this.receive(success_callback, error_callback, this._timeout)
            }), this._error_callback)
          }));
          break;
        case"002":
        case"004":
          this._receive_loop = false;
          break;
        case"003":
          this.receive(success_callback, error_callback, this._timeout);
          break;
        default:
          if (log) {
            log.error("(push::_receive_success)", "Error not managed : " + data.response.fault.faultcode + "," + data.response.fault.faultstring)
          }
          break
      }
      error_code = data.response.fault.faultcode;
      error_string = data.response.fault.faultstring;
      error_details = data.response.fault.faultdetails
    } else if (typeof data.response.data == "undefined") {
      error_code = "501";
      error_string = "Malformed response from server. Data missing.";
      error_details = JSON.stringify(data)
    } else {
      this._trigger("push.connectivityUp");
      if (this._receive_loop) {
        if (log) {
          log.debug("(push::_receive_success)", "Success, loop receive")
        }
        this.receive(success_callback, error_callback, this._timeout)
      } else {
        if (log) {
          log.debug("(push::_receive_success)", "Success, stopped receive")
        }
      }
      if (success_callback && data.response.data) success_callback(data.response.data);
      return true
    }
    if (log) {
      log.error("(push::_receive_success)", "Error (" + error_code + ")," + error_string)
    }
    if (error_callback) {
      error_callback(error_code, error_string, url, error_details, "receive")
    }
    return false
  };
  this._receive_error = function (error, status, jqXhr, url, callback, log) {
    if (this._receive_loop) {
      if (log) {
        log.error("(push::_receive_error)", "Error (" + status + ")," + error + ", retrying receive ...")
      }
      this._trigger("push.connectivityDown");
      setTimeout(this.bind(function () {
        this.receive(this._notification_callback, this._error_callback, this._timeout)
      }), this._receive_retry_tempo)
    } else {
      if (log) {
        log.error("(push::_receive_error)", "Error (" + status + ")," + error + ", stopped receive")
      }
    }
    if (callback) {
      callback(status, error, url, jqXhr.responseText, "receive")
    }
  };
  this.send = function (id, device, data, success_callback, error_callback, timeout) {
    timeout = timeout || this._timeout;
    if (timeout > 0) this._timeout = timeout;
    var params = data, service = null;
    params.method = "send";
    params.id = id;
    params.device = device;
    wsajax({
      method: "POST",
      type: "json",
      url: this._server + "/push/send",
      headers: {"Content-type": "application/json;charset=UTF-8", Authorization: "Bearer " + this._access_token},
      data: JSON.stringify(params),
      onsuccess: this.bind(function (data, status, jqXhr) {
        this._send_success(data, status, jqXhr, success_callback, error_callback, log)
      }),
      onerror: function (error, status, jqXhr, url) {
        this._send_error(error, status, jqXhr, url, error_callback, log)
      }
    })
  };
  this._send_success = function (data, status, jqXhr, success_callback, error_callback, log) {
    var url = "", error_code = "", error_string = "", error_details = "";
    if (!data) {
      error_code = "501";
      error_string = "Empty response from server";
      error_details = ""
    } else if (typeof data.response == "undefined") {
      error_code = "501";
      error_string = "Malformed response from server. Response level missing.";
      error_details = JSON.stringify(data)
    } else if (typeof data.response.fault != "undefined") {
      error_code = data.response.fault.faultcode;
      error_string = data.response.fault.faultstring;
      error_details = data.response.fault.faultdetails
    } else if (typeof data.response.data == "undefined") {
      error_code = "501";
      error_string = "Malformed response from server. Data missing.";
      error_details = JSON.stringify(data)
    } else {
      if (log) {
        log.debug("(push._send_success)", "Success, stopped send")
      }
      if (success_callback && data.response.data) {
        success_callback(data.response.data)
      }
      return true
    }
    if (log) {
      log.error("(push::_send_success)", "Error (" + error_code + ")," + error_string)
    }
    if (error_callback) {
      error_callback(error_code, error_string, url, error_details, "send")
    }
    return false
  };
  this._send_error = function (error, status, jqXhr, url, callback, log) {
    if (log) {
      log.error("(push::_send_error)", "Error (" + status + ")," + error)
    }
    if (callback) {
      callback(status, error, url, jqXhr.responseText, "send")
    }
  };
  this._trigger = function (event) {
    if (this._lastEventTriggered != event) {
      this._lastEventTriggered = event;
      log.debug("(push::_trigger)", event)
    }
  }
}

abcde.prototype = new classThatManageContextInCallback;

function abcde(server, log, timeout) {
  this._server = server;
  this._obc_server = server;
  this._log = log || null;
  this._timeout = timeout || 3e4;
  this._token = null;
  this._id = null;
  this._device = null;
  this._channel = null;
  this._receive_timeout = this._timeout / 1e3;
  this._receive_loop = false;
  this._receive_retry_tempo = 15e3;
  this._lastEventTriggered = "";
  this.token = function (token) {
    this._token = token
  };
  this.getService = function (serviceId, onSuccess, onError) {
    wsajax({
      method: "GET",
      type: "json",
      url: this._obc_server + "/services/" + serviceId,
      headers: {"Content-type": "application/json;charset=UTF-8"},
      onsuccess: this.bind(function (result) {
        if (typeof result.abcde_server != "undefined" && result.abcde_server.length > 0) {
          this._server = result.abcde_server
        }
        onSuccess(result)
      }),
      onerror: onError
    })
  };
  this.postUser = function (data, onSuccess, onError) {
    wsajax({
      method: "POST",
      type: "json",
      url: this._obc_server + "/users",
      headers: {"Content-type": "application/json;charset=UTF-8"},
      data: JSON.stringify(data),
      onsuccess: onSuccess,
      onerror: onError
    })
  };
  this.patchUser = function (user, onSuccess, onError) {
    wsajax({
      method: "PATCH",
      type: "json",
      url: this._server + "/users",
      headers: {"Content-type": "application/json;charset=UTF-8", Authorization: "Bearer " + this._token},
      data: JSON.stringify(user),
      onsuccess: onSuccess,
      onerror: onError
    })
  };
  this.postSubscriptions = function (serviceId, onSuccess, onError) {
    wsajax({
      method: "POST",
      type: "json",
      url: this._server + "/subscriptions/" + serviceId,
      headers: {"Content-type": "application/json;charset=UTF-8", Authorization: "Bearer " + this._token},
      data: JSON.stringify({}),
      onsuccess: onSuccess,
      onerror: onError
    })
  };
  this.postPushChannels = function (deviceId, onSuccess, onReceive, onError, timeout) {
    wsajax({
      method: "POST",
      type: "json",
      url: this._server + "/pushChannels",
      headers: {"Content-type": "application/json;charset=UTF-8", Authorization: "Bearer " + this._token},
      data: JSON.stringify({type: "lp", deviceId: deviceId}),
      onsuccess: this.bind(function (result) {
        this._log.log("(abcde::postPushChannels::onsuccess)", JSON.stringify(result));
        this._push = new push(result.response.endpoint, this._log);
        this._push._server = result.response.server.replace("http:", "https:");
        this._push._access_token = this._token;
        this._push._type = "lp";
        this._push._channel = result.response.channel;
        this._push._receive_timeout = result.response.receive_timeout;
        this._push._receive_loop = true;
        onSuccess(result);
        this._push.receive(onReceive, onError, result.response.timeout * 1e3)
      }),
      onerror: onError
    })
  };
  this.postMessages = function (serviceId, message, onSuccess, onError) {
    if (message !== null && typeof message === "object") {
      message = JSON.stringify(message)
    }
    wsajax({
      method: "POST",
      type: "json",
      url: this._server + "/messages/services/" + serviceId,
      headers: {"Content-type": "application/json;charset=UTF-8", Authorization: "Bearer " + this._token},
      data: message,
      onsuccess: onSuccess,
      onerror: onError
    })
  };
  this.postContents = function (serviceId, content, onSuccess, onError) {
    wsajax({
      method: "POST",
      type: "json",
      url: this._obc_server + "/contents/" + serviceId,
      headers: {"Content-type": "application/json;charset=UTF-8", Authorization: "Bearer " + this._token},
      data: content,
      onsuccess: onSuccess,
      onerror: onError
    })
  }
}

dialog.prototype = new classThatManageContextInCallback;

function dialog(title, icon, message, leftButtonText, rightButtonText, donotAskAnymoreValue, donotAskAnymoreText, callbackValid, callbackCancel, callbackDonotAskAnymore, callbackInit) {
  this.donotAskAnymore = false;
  this.viewId = Math.random().toString(36).substring(7);
  this.view = '<div id="dialog_' + this.viewId + '" class="dialog">' + '<div id="dialogBg_' + this.viewId + '" class="dialogBg"></div>' + '<div id="dialogContainer_' + this.viewId + '" class="dialogContainer">' + '<div id="dialogHeader_' + this.viewId + '" class="dialogHeader">' + '<div id="dialogHeaderIcon_' + this.viewId + '" class="dialogHeaderIcon"></div>' + '<div id="dialogHeaderTitle_' + this.viewId + '" class="dialogHeaderTitle"></div>' + "</div>" + '<div id="dialogContent_' + this.viewId + '" class="dialogContent">' + '<div id="dialogContentText_' + this.viewId + '" class="dialogContentText"></div>' + '<div id="dialogDonotAskAnymore_' + this.viewId + '" class="dialogDonotAskAnymore">' + '<input type="checkbox" id="dialogDonotAskAnymoreValue_' + this.viewId + '" class="dialogDonotAskAnymoreValue" />' + '<div id="dialogDonotAskAnymoreLabel_' + this.viewId + '" class="dialogDonotAskAnymoreLabel"></div>' + "</div>" + "</div>" + '<div id="dialogFooter_' + this.viewId + '" class="dialogFooter">' + '<div id="dialogLeftButton_' + this.viewId + '" class="dialogLeftButton">' + '<div id="dialogLeftButtonLabel_' + this.viewId + '" class="dialogLeftButtonLabel"></div>' + "</div>" + '<div id="dialogRightButton_' + this.viewId + '" class="dialogRightButton">' + '<div id="dialogRightButtonLabel_' + this.viewId + '" class="dialogRightButtonLabel"></div>' + "</div>" + "</div>" + "</div>" + "</div>";
  this.construct = function () {
    $("body").append(this.view);
    $("#dialogHeaderTitle_" + this.viewId).html(title);
    $("#dialogHeaderIcon_" + this.viewId).css("background-image", "none");
    $("#dialogContentText_" + this.viewId).html(message);
    $("#dialogLeftButtonLabel_" + this.viewId).html(leftButtonText);
    $("#dialogRightButtonLabel_" + this.viewId).html(rightButtonText);
    if (leftButtonText && leftButtonText) {
      $("#dialogLeftButton_" + this.viewId).addClass("dialogLeftButton2");
      $("#dialogRightButton_" + this.viewId).addClass("dialogRightButton2")
    }
    if (donotAskAnymoreValue) {
      $("#dialogDonotAskAnymoreLabel_" + this.viewId).html(donotAskAnymoreText);
      $("#dialogDonotAskAnymore_" + this.viewId).show()
    } else {
      $("#dialogDonotAskAnymore_" + this.viewId).hide()
    }
    $("#dialogLeftButton_" + this.viewId).off();
    $("#dialogLeftButton_" + this.viewId).on("click", this.bind(function () {
      if (typeof callbackCancel == "function") callbackCancel(true);
      this.destroy()
    }));
    $("#dialogRightButton_" + this.viewId).off();
    $("#dialogRightButton_" + this.viewId).on("click", this.bind(function () {
      if (typeof callbackDonotAskAnymore == "function") callbackDonotAskAnymore(!this.donotAskAnymore);
      if (typeof callbackValid == "function") callbackValid(false);
      this.destroy()
    }));
    $("#dialog_" + this.viewId).off();
    $("#dialog_" + this.viewId).on("keyup", this.bind(function (event) {
      if (event.keyCode == 13) {
        $("#dialogRightButton_" + this.viewId).click()
      }
      if (event.keyCode == 27) {
        $("#dialogLeftButton_" + this.viewId).click()
      }
    }));
    $("#dialogDonotAskAnymore_" + this.viewId).off();
    $("#dialogDonotAskAnymore_" + this.viewId).on("click", this.bind(function (event) {
      this.donotAskAnymore = $("#dialogDonotAskAnymoreValue_" + this.viewId).attr("checked") == "checked";
      $("#dialogDonotAskAnymoreValue_" + this.viewId).attr("checked", this.donotAskAnymore)
    }));
    if (icon) {
      lefil.services.preload.getImg(icon, this.bind(function (data, context) {
        if (typeof data == "string" && data.substr(0, 5) == "blob:") {
          $("#dialogHeaderIcon_" + this.viewId).css("background-image", "url(" + data + ")")
        }
        this.show()
      }), {}, false, 1, null, this.bind(function (data, context) {
        if (typeof callbackInit == "function") callbackInit();
        this.show()
      }))
    } else {
      if (typeof callbackInit == "function") callbackInit();
      this.show()
    }
  };
  this.destroy = function () {
    this.hide();
    $("#dialog_" + this.viewId).remove();
    delete this
  };
  this.show = function () {
    $("#dialog_" + this.viewId).fadeIn(300)
  };
  this.hide = function () {
    $("#dialog_" + this.viewId).fadeOut(300)
  };
  this.construct()
}

user_service.prototype = new classThatManageContextInCallback;
user_service.prototype.constructor = user_service;

function user_service(server, log, timeout) {
  this._server = server;
  this._log = log || null;
  this._timeout = timeout || 3e4;
  this._appkey = config.appKey;
  this._timeout = timeout || 3e4;
  this._user = null;
  this.construct = function () {
  };
  this.destroy = function () {
  };
  this.onConnect = function (onConnect) {
    this._onConnect = onConnect || this._onConnect
  };
  this.onReconnect = function (onReconnect) {
    this._onReconnect = onReconnect || this._onReconnect
  };
  this.onRenew = function (onRenew) {
    this._onRenew = onRenew || this._onRenew
  };
  this.onDisconnect = function (onDisconnect) {
    this._onDisconnect = onDisconnect || this._onDisconnect
  };
  this.isConnected = function () {
    return this.session_id != ""
  };
  this.resize = function () {
  };
  this.showInfosMenu = function (callback_ok) {
    $("#user_infos_name").html(this.var("name"));
    $("#user_infos_id").html(this.var("user"));
    $("#user_disconnect_button").html(lb.lang.userDisconnectButton);
    $("#user_buttons_container").show();
    $("#user_infos_container").show();
    $("#user_disconnect_button").off();
    $("#user_disconnect_button").on("click", this.bind(function (event) {
      this.disconnect()
    }));
    $("#user_infos_name").off();
    $("#user_infos_name").on("click", this.bind(function (event) {
      this.ident()
    }));
    $("#user_infos_id").off();
    $("#user_infos_id").on("click", this.bind(function (event) {
      this.ident()
    }))
  };
  this.hideInfosMenu = function (callback_ok) {
    $("#user_disconnect_button").off();
    $("#user_buttons_container").hide();
    $("#user_infos_container").hide()
  };
  this.showButtonConnect = function (callback_ok) {
    $("#user_connect_text").html(lb.lang.userConnectText);
    $("#user_connect_button").html(lb.lang.userConnectButton);
    $("#user_connect_button").off();
    $("#user_connect_button").on("click", this.bind(function (event) {
      this.connect()
    }));
    $("#user_buttons_container").show();
    $("#user_connect_container").show();
    if (this.var("state") == "anonymous") {
      $("#user_connect").fadeIn(300)
    }
  };
  this.hideButtonConnect = function (callback_ok) {
    $("#user_connect_button").off();
    $("#user_buttons_container").hide();
    $("#user_connect_container").hide()
  };
  this.showButtonRegister = function (callback_ok) {
    $("#user_register_text").html(lb.lang.userCreateText);
    $("#user_register_button").html(lb.lang.userCreateButton);
    $("#user_register_button").off();
    $("#user_register_button").on("click", this.bind(function (event) {
      this.register()
    }));
    $("#user_buttons_container").show();
    $("#user_register_container").show();
    if (this.var("state") == "anonymous") {
      $("#user_register").fadeIn(300)
    }
  };
  this.hide = function () {
    $("#connect_connect").fadeOut(300);
    $("#connect_register").fadeOut(300);
    $("#connect_container").fadeOut(300)
  };
  this.init = function (user, callback_ok) {
    this._log("(user::init)", this.var("state"), this.var("user"));
    this._user = user;
    if (this._user.uid == undefined || this._user.did == undefined || this._user.user == undefined || this._user.token == undefined) {
      this._user = this.var("user")
    }
    if (this._user.uid == undefined || this._user.did == undefined || this._user.user == undefined || this._user.token == undefined) {
      this._log("(user_service::init) create a new user")
    }
    if (this._uservar("user")) {
      this.var("state", "anonymous");
      this.var("user", "");
      this.var("token", "");
      this.var("country", lb.var.country);
      this.var("nbConnexion", 0);
      this.var("firstConnexion", (new Date).getTime());
      this.var("storeOnDevice", true)
    }
    if (typeof this._onInit == "function") this._onInit();
    if (this.var("user") && this.var("token")) {
      this.connect()
    }
  };
  this.register = function (callback_ok) {
    new dialog(lb.lang.userRegisterMobileLabel, lb.lang.userRegisterMobileIcon, lb.lang.userRegisterMobileMessage, lb.lang.cancel, lb.lang.valid, false, "", this.bind(function () {
      this.check($("#userRegisterMobile").val(), callback_ok)
    }), function () {
    })
  };
  this.check = function (number, callback_ok) {
    var webservice = new phplibwebservice(this.ws_server, "json");
    webservice.get({
      ws_app_key: this.ws_appkey,
      ws_service: this.ws_service,
      ws_method: "checkIdentity",
      appkey: this.ws_appkey,
      identity: number,
      country: lb.var.country,
      from: lb.const.servName.charAt(0).toUpperCase() + lb.const.servName.slice(1),
      subject: lb.const.servName.charAt(0).toUpperCase() + lb.const.servName.slice(1),
      message: lb.lang.userCheckCodeSMS
    }, this.bind(function (response) {
      lb.serv.contacts.connect();
      new dialog(lb.lang.userCheckCodeLabel, lb.lang.userCheckCodeIcon, lb.lang.userCheckCodeMessage, lb.lang.cancel, lb.lang.valid, false, "", this.bind(function () {
        this.token(number, $("#userCheckCode").val(), callback_ok)
      }), function () {
      })
    }), function (code, status, jqXhr, url, callback, method) {
      new dialog(lb.lang["userCheckCodeErrorTitle_" + method.replace("::", "_") + "_" + code] || lb.lang.error, "", lb.lang["userCheckCodeErrorMessage_" + method.replace("::", "_") + "_" + code] || lb.lang.error_unkown + "<br>" + method + "<br>" + code, "", lb.lang.ok, false)
    })
  };
  this.token = function (number, code, callback_ok) {
    var webservice = new phplibwebservice(this.ws_server, "json");
    webservice.get({
      ws_app_key: this.ws_appkey,
      ws_service: this.ws_service,
      ws_method: "createAccount",
      appkey: this.ws_appkey,
      identity: number,
      country: lb.var.country,
      securitycode: code
    }, this.bind(function (response) {
      this.var("state", "connected");
      this.var("uid", response.uid);
      this.var("user", response.user);
      this.var("token", response.token);
      this.var("country", response.country);
      this.checkIdent(callback_ok)
    }), function (error_code, error_string, url, error_details) {
    })
  };
  this.reToken = function (number, token, onSuccess, onError) {
    var webservice = new phplibwebservice(this.ws_server, "json");
    webservice.get({
      ws_app_key: this.ws_appkey,
      ws_service: this.ws_service,
      ws_method: "reToken",
      appkey: this.ws_appkey,
      identity: number,
      token: token
    }, this.bind(function (response) {
      this.var("state", "connected");
      this.var("token", response.token);
      if (typeof onSuccess == "function") onSuccess()
    }), function (error) {
      if (typeof onError == "function") onError(error)
    })
  };
  this.checkIdent = function (callback_ok) {
    lb.serv.contacts.search(this.var("user"), "search", this.bind(function (data) {
      if (data.name) {
        if (data.name) this.var("name", data.name);
        if (data.type) this.var("type", data.type);
        if (data.address1) this.var("address1", data.address1);
        if (data.address2) this.var("address2", data.address2);
        if (data.zipcode) this.var("zipcode", data.zipcode);
        if (data.city) this.var("city", data.city);
        if (data.locX) this.var("locX", data.locX);
        if (data.locY) this.var("locY", data.locY);
        if (data.activity) this.var("activity", data.activity);
        this.connect("", callback_ok)
      } else {
        this.ident(callback_ok)
      }
    }), this.bind(function (error) {
    }))
  };
  this.ident = function (callback_ok) {
    new dialog(lb.lang.userIdentLabel, lb.lang.userIdentIcon, lb.lang.userIdentMessage, lb.lang.cancel, lb.lang.valid, false, "", this.bind(function () {
      this.var("name", $("#userIdentName").val());
      this.var("address1", $("#userIdentAddress1").val());
      this.var("address2", $("#userIdentAddress2").val());
      this.var("zipcode", $("#userIdentZipcode").val());
      this.var("city", $("#userIdentCity").val());
      this.var("activity", $("#userIdentActivity").val());
      lb.serv.contacts.set({
        name: this.var("name"),
        address1: this.var("address1"),
        address2: this.var("address2"),
        zipcode: this.var("zipcode"),
        city: this.var("city"),
        activity: this.var("activity")
      }, function () {
      }, function () {
      });
      this.connect("", callback_ok)
    }), this.bind(function () {
      this.connect("", callback_ok)
    }), null, this.bind(function () {
      $("#userIdentName").val(this.var("name"));
      $("#userIdentAddress1").val(this.var("address1"));
      $("#userIdentAddress2").val(this.var("address2"));
      $("#userIdentZipcode").val(this.var("zipcode"));
      $("#userIdentCity").val(this.var("city"));
      $("#userIdentActivity").val(this.var("activity"))
    }))
  };
  this.connect = function (message, callback_ok) {
    if (this.var("user") && this.var("token")) {
      if (typeof this._onConnect == "function") this._onConnect();
      if (typeof callback_ok == "function") callback_ok()
    } else {
      this.register(callback_ok)
    }
  };
  this.disconnect = function () {
    new dialog(lb.lang.userDisconnectLabel, lb.lang.userDisconnectIcon, lb.lang.userDisconnectMessage, lb.lang.cancel, lb.lang.valid, false, "", this.bind(function () {
      if (this.var("state") != "connected") return false;
      this.var("state", "disconnecting");
      this.var("country", lb.var.country);
      this.var("uid", "");
      this.var("user", "");
      this.var("token", "");
      this.var("name", "");
      this.var("type", "");
      this.var("address1", "");
      this.var("address2", "");
      this.var("zipcode", "");
      this.var("city", "");
      this.var("locX", "");
      this.var("locY", "");
      this.var("state", "disconnected");
      if (typeof this._onDisconnect == "function") this._onDisconnect()
    }), function () {
    })
  };
  this.var = function (name, value) {
    if (value == undefined) {
      if (typeof window.localStorage[name] != "undefined") {
        return window.localStorage[name]
      } else {
        return undefined
      }
    } else {
      if (window.localStorage) {
        window.localStorage[name] = value;
        return true
      } else {
        return false
      }
    }
  };
  this.construct()
}

applicationAbcde.prototype = new classThatManageContextInCallback;

function applicationAbcde() {
  this.args = {
    botid: typeof WEBAPP_ABCDE_BOTID != "undefined" ? WEBAPP_ABCDE_BOTID : "",
    serviceid: typeof WEBAPP_ABCDE_BOTID != "undefined" ? WEBAPP_ABCDE_BOTID : "",
    uid: undefined,
    did: undefined,
    user: undefined,
    token: undefined
  };
  this.langs = {fr: {phAbcdeFormText: "Votre message ..."}, en: {phAbcdeFormText: "Type a message ..."}};
  this.user = {lang: (window.navigator.userLanguage || window.navigator.language).substring(0, (window.navigator.userLanguage || window.navigator.language).indexOf("-")) || (window.navigator.userLanguage || window.navigator.language)};
  this.lang = this.langs[this.user.lang] != undefined ? this.langs[this.user.lang] : this.langs["en"];
  this.startime = new Date;
  this.appliStarted = false;
  this.console = {
    log: function (method = "", text = "") {
    }, warn: function (method = "", text = "") {
    }, error: function (method = "", text = "") {
    }, debug: function (method = "", text = "") {
    }
  };
  this.serviceId = null;
  this.service = null;
  this.srv = {install: null, params: null, location: null, abcde: new abcde(WEBAPP_ABCDE_SERVER, this.console)};
  this.services = [];
  this.messages = [];
  this.isTyping = null;
  this.mX = 0;
  this.mY = 0;
  this.elX = 0;
  this.elY = 0;
  this.init = function () {
    this.user.uid = this.args.uid;
    this.user.did = this.args.did;
    this.user.user = this.args.user;
    this.user.token = this.args.token;
    document.getElementById("chatAbcdeButton").addEventListener("click", this.bind(function (event) {
      this.openChatView()
    }));
    document.getElementById("chatAbcdeViewTitle").addEventListener("mousedown", this.bind(function (event) {
      this.moveChatView(event)
    }));
    document.getElementById("chatAbcdeViewMin").addEventListener("click", this.bind(function (event) {
      this.minChatView(event)
    }));
    document.getElementById("chatAbcdeViewClose").addEventListener("click", this.bind(function (event) {
      this.closeChatView(event)
    }));
    document.getElementById("chatAbcdeViewFormMore").addEventListener("click", this.bind(function (event) {
      this.clickMore(event)
    }));
    document.getElementById("chatAbcdeFileUploader").addEventListener("change", this.bind(function (event) {
      this.clickUpload(event)
    }));
    document.getElementById("chatAbcdeViewFormSend").addEventListener("click", this.bind(function (event) {
      this.clickSend(event)
    }));
    document.getElementById("chatAbcdeViewFormText").addEventListener("keyup", this.bind(function (event) {
      var key = event.which || event.keyCode;
      if (key === 13) {
        this.clickSend(event)
      }
    }));
    if (this.args.botid != undefined && this.args.botid && this.args.serviceid != undefined && this.args.serviceid) {
      this.botId = this.args.botid;
      this.serviceId = this.args.serviceid;
      this.initChatButton(this.args.serviceid)
    } else if (this.user.uid == undefined || this.user.did == undefined || this.user.user == undefined || this.user.token == undefined) {
    } else {
      this.srv.abcde.token(this.user.token);
      this.pushStart(this.bind(function () {
        if (this.args.serviceid) {
          this.serviceId = this.args.serviceid;
          this.openChatView(this.args.serviceid)
        } else {
          this.openChatList()
        }
      }))
    }
  };
  this.authenticate = function (callback) {
    return true
  };
  this.pushStart = function (onSuccess = null, onError = null) {
    this.srv.abcde.postPushChannels(this.user.did, onSuccess, this.bind(function (data) {
      this.pushReceive(data)
    }), function (error) {
    });
    return true
  };
  this.pushReceive = function (data) {
    var notif = JSON.parse(decodeURIComponent(data.data));
    switch (notif.type) {
      case"update":
        this.services[notif.sender] = Object.assign(this.services[notif.sender], notif.content);
        if (this.services[notif.sender].opened) {
          this.displayService(this.services[notif.sender])
        }
        break;
      case"message":
        app.storeMessage(notif.type, notif.sender, notif.content);
        break;
      default:
        break
    }
    return true
  };
  this.pushStop = function (onSuccess = null, onError = null) {
    this.srv.abcde._push._receive_loop = false;
    return true
  };
  this.storeMessage = function (type, serviceId, content) {
    if (!this.messages[serviceId]) {
      this.messages[serviceId] = []
    }
    this.messages[serviceId].push(content);
    if (this.services[serviceId] && this.services[serviceId].opened) {
      this.displayMessage(serviceId, content)
    }
    return true
  };
  this.openChatList = function () {
    return true
  };
  this.openChatList = function () {
    return true
  };
  this.getService = function (serviceId, onSuccess, onError) {
    if (!this.services[serviceId]) {
      this.srv.abcde.getService(serviceId, this.bind(function (result) {
        this.services[serviceId] = result;
        onSuccess(this.services[serviceId])
      }), this.bind(function (error) {
        onError(error)
      }))
    } else {
      onSuccess(this.services[serviceId])
    }
  };
  this.initChatButton = function () {
    this.getService(this.serviceId, this.bind(function (result) {
      this.service = result;
      if (typeof this.service._id != "undefined" && this.service._id != this.serviceId) {
        this.serviceId = this.service._id
      }
      this.services[this.serviceId] = this.service;
      this.services[this.serviceId].opened = false;
      this.displayService(this.services[this.serviceId]);
      this.openChatButton()
    }), this.bind(function (error) {
    }));
    return true
  };
  this.openChatButton = function () {
    document.getElementById("chatAbcdeButton").style.display = "inline-block";
    return true
  };
  this.closeChatButton = function () {
    document.getElementById("chatAbcdeButton").style.display = "none";
    return true
  };
  this.openChatView = function () {
    if (navigator.userAgent.match(/iPad;.*CPU.*OS 7_\d/i)) {
      if (window.innerHeight == 672) {
      }
    }
    document.getElementById("chatAbcdeViewFormSend").setAttribute("action-serviceid", this.serviceId);
    document.getElementById("chatAbcdeViewFormSend").setAttribute("action-type", "text");
    document.getElementById("chatAbcdeViewFormSend").setAttribute("action-value", "");
    document.getElementById("chatAbcdeViewFormText").setAttribute("action-serviceid", this.serviceId);
    document.getElementById("chatAbcdeViewFormText").setAttribute("action-type", "text");
    document.getElementById("chatAbcdeViewFormText").setAttribute("action-value", "");
    document.getElementById("chatAbcdeViewFormText").setAttribute("placeholder", this.lang["phAbcdeFormText"]);
    document.getElementById("chatAbcdeView").style.display = "inline-block";
    document.getElementById("chatAbcdeView").style.visibility = "visible";
    document.getElementById("chatAbcdeView").style.opacity = 1;
    document.getElementById("chatAbcdeViewMessagesList").scrollTop = document.getElementById("chatAbcdeViewMessagesList").scrollHeight;
    if (!this.services[this.serviceId].opened) {
      this.services[this.serviceId].opened = true;
      this.srv.abcde.postUser({mode: "anonymous"}, this.bind(function (result) {
        this.user.id = result.user;
        this.user.uid = result.uid;
        this.user.did = result.did;
        this.user.token = result.token;
        this.srv.abcde.token(this.user.token);
        this.srv.abcde.patchUser({
          firstName: "anonymous",
          lastName: "anonymous",
          address: "somewhere",
          email: "anonymous@somewhere",
          lang: this.user.lang,
          lat: 0,
          long: 0
        }, this.bind(function (result) {
          this.pushStart(this.bind(function (result) {
            this.services[this.serviceId].opened = true;
            this.closeChatButton();
            this.srv.abcde.postSubscriptions(this.serviceId, this.bind(function (result) {
              this.srv.abcde.postMessages(this.serviceId, {type: "postBack", value: "session_start"})
            }), this.bind(function (error) {
            }))
          }))
        }), this.bind(function (error) {
        }))
      }), this.bind(function (result) {
      }))
    } else {
      document.getElementById("chatAbcdeView").style.display = "inline-block"
    }
    return true
  };
  this.minChatView = function () {
    this.services[this.serviceId].opened = true;
    document.getElementById("chatAbcdeView").style.display = "none";
    this.openChatButton();
    this.srv.abcde.postMessages(this.serviceId, {type: "postBack", value: "session_pause"});
    return true
  };
  this.closeChatView = function () {
    this.services[this.serviceId].opened = false;
    document.getElementById("chatAbcdeView").style.display = "none";
    document.getElementById("chatAbcdeViewMessagesList").innerHTML = "";
    this.openChatButton();
    this.pushStop();
    this.srv.abcde.postMessages(this.serviceId, {
      type: "postBack",
      value: "session_close"
    }, this.bind(function (result) {
      document.getElementById("chatAbcdeViewMessagesList").innerHTML = ""
    }), this.bind(function (error) {
      document.getElementById("chatAbcdeViewMessagesList").innerHTML = ""
    }));
    return true
  };
  this.moveChatView = function (event) {
    event.stopPropagation();
    event.preventDefault();
    var el = document.getElementById("chatAbcdeView");
    this.mX = event.clientX;
    this.mY = event.clientY;
    this.elX = el.offsetLeft;
    this.elY = el.offsetTop;
    var mv = function (click_count) {
      var h1 = function (event) {
        event.stopPropagation();
        event.preventDefault();
        var dX = this.mX - event.clientX;
        var dY = this.mY - event.clientY;
        var el = document.getElementById("chatAbcdeView");
        el.style.left = this.elX - dX + "px"
      };
      return h1
    }();
    var st = function () {
      var h2 = function (event) {
        event.stopPropagation();
        event.preventDefault();
        document.removeEventListener("mousemove", mv)
      };
      return h2
    }();
    document.addEventListener("mousemove", mv);
    document.addEventListener("mouseup", st);
    return true
  };
  this.displayService = function (service) {
    document.getElementById("chatAbcdeViewTitle").style.backgroundColor = "#" + service.color;
    document.getElementById("chatAbcdeViewTitle").style.color = "white";
    document.getElementById("chatAbcdeViewMin").style.color = "white";
    document.getElementById("chatAbcdeViewClose").style.color = "white";
    document.getElementById("chatAbcdeViewTitle").innerHTML = service.name;
    if (service.status && service.status.value) {
      document.getElementById("chatAbcdeViewStatus").src = "data:text/html;charset=utf-8," + encodeURIComponent("<html>" + "<head><style>html{font-family:sans-serif;}</style></head>" + "<body>" + service.status.value + "</body>" + "</html>")
    }
    return true
  };
  this.clickMore = function (event) {
    element = event.target;
    document.getElementById("chatAbcdeFileUploader").click()
  };
  this.clickSend = function (event) {
    element = event.target;
    if (document.getElementById("chatAbcdeViewFormText").value) {
      var message = {messageId: uuidv4(), type: element.getAttribute("action-type")};
      switch (element.getAttribute("action-type")) {
        case"text":
          message.value = document.getElementById("chatAbcdeViewFormText").value;
          this.displayMessage(element.getAttribute("action-serviceId"), message, "sentTextAbcde");
          app.srv.abcde.postMessages(element.getAttribute("action-serviceId"), message);
          break;
        case"formData":
          message.value = element.getAttribute("action-value");
          message.data = {
            name: element.getAttribute("action-value"),
            value: document.getElementById("chatAbcdeViewFormText").value
          };
          app.srv.abcde.postMessages(element.getAttribute("action-serviceId"), message);
          break;
        default:
          break
      }
      document.getElementById("chatAbcdeViewFormText").value = ""
    }
  };
  this.clickButton = function (button) {
    var message = {messageId: uuidv4(), type: "text"};
    switch (button.getAttribute("action-type")) {
      case"postBack":
        message.value = button.getAttribute("action-label");
        this.displayMessage(button.getAttribute("action-serviceId"), message, "sentTextAbcde");
        app.srv.abcde.postMessages(button.getAttribute("action-serviceId"), {
          type: button.getAttribute("action-type"),
          value: button.getAttribute("action-value"),
          context: button.getAttribute("action-context"),
          messageId: button.getAttribute("action-messageId")
        });
        break;
      case"url":
        var onClick = 'location.href="' + button.getAttribute("action-value") + '"';
        window.open(button.getAttribute("action-value"));
        break;
      case"call":
        var onClick = 'location.href="tel:' + button.getAttribute("action-value") + '"';
        window.open("tel:" + button.getAttribute("action-value"));
        break;
      default:
        break
    }
  };
  this.clickUpload = function (event) {
    element = event.target;
    var files = event.target.files || event.dataTransfer.files;
    if (files.length) {
      loadImage(files[0], this.bind(function (canvas, metadata) {
        var data = canvas.toDataURL("image/jpeg");
        app.srv.abcde.postContents(this.botId, JSON.stringify({
          name: files[0].name,
          size: data.length,
          dataUrl: data
        }), this.bind(function (result) {
          var message = {
            messageId: uuidv4(),
            type: "file",
            value: result.fileName,
            context: "",
            file: {
              type: "image",
              fileUri: result.fileUrl,
              fileName: result.fileName,
              fileSize: data.length,
              fileType: "image/jpeg"
            }
          };
          this.displayMessage(this.serviceId, message, "sentFileAbcde");
          app.srv.abcde.postMessages(this.serviceId, message)
        }), this.bind(function (error) {
        }))
      }), {maxWidth: 400, meta: true, orientation: true, canvas: true})
    }
  };
  this.displayMessage = function (serviceId, message, type) {
    type = type || "receivedAbcde";
    var quickreplies = document.getElementById("buttonSection");
    if (quickreplies) {
      quickreplies.parentNode.removeChild(quickreplies)
    }
    this.stopIstyping();
    document.getElementById("chatAbcdeViewFormSend").setAttribute("action-type", "text");
    document.getElementById("chatAbcdeViewFormSend").setAttribute("action-value", "");
    var html = "";
    var label = "";
    switch (message.type) {
      case"text":
        html = message.value;
        break;
      case"image":
        html = "<img style='width:100%;border-radius:20px;' src='" + message.src + '\' onload=\'document.getElementById("chatAbcdeViewMessagesList").scrollTop = document.getElementById("chatAbcdeViewMessagesList").scrollHeight;\'>';
        break;
      case"file":
        html = "<img style='width:100%;border-radius:20px;' src='" + message.file.fileUri + '\' onload=\'document.getElementById("chatAbcdeViewMessagesList").scrollTop = document.getElementById("chatAbcdeViewMessagesList").scrollHeight;\'>';
        break;
      case"button":
        html = "<div class='buttonSection'>\n";
        for (var index in message.buttons) {
          var button = message.buttons[index];
          label = button.action.type == "url" ? button.context : button.label;
          html += '<button class="button-default" action-label="' + label + '" action-serviceId="' + serviceId + '" action-type="' + button.action.type + '" action-value="' + button.action.value + '" onClick="javascript:app.clickButton(this)">\n' + button.value + "\n" + "</button>\n"
        }
        html += "</div>\n";
        break;
      case"paiement":
        break;
      case"composite":
        html = "";
        label = "";
        var separator = "";
        for (var index in message.sections) {
          var section = message.sections[index];
          html += separator + "\n";
          switch (section.type) {
            case"text":
              html += section.value.replace(/###/g, "<br>") + "\n";
              break;
            case"image":
              html += "<img src='" + section.src + "' style='width: 100%;'>\n";
              break;
            case"button":
              html += "<div class='buttonSection'>\n";
              for (var index in section.buttons) {
                var button = section.buttons[index];
                label = button.action.type == "url" ? button.context : button.label;
                html += '<button class="button-default" action-label="' + label + '" action-serviceId="' + serviceId + '" action-type="' + button.action.type + '" action-value="' + button.action.value + '" onClick="javascript:app.clickButton(this)">\n' + button.value + "\n" + "</button>\n"
              }
              html += "</div>\n";
              break;
            case"quickreplies":
              html += "<div id='buttonSection' class='quickreplySection'>\n";
              for (var index in section.buttons) {
                var button = section.buttons[index];
                label = button.action.type == "url" ? button.context : button.label;
                html += '<button class="button-default" action-label="' + label + '" action-serviceId="' + serviceId + '" action-type="' + button.action.type + '" action-value="' + button.action.value + '" onClick="javascript:app.clickButton(this)">\n' + button.value + "\n" + "</button>\n"
              }
              html += "</div>\n";
              break;
            case"typing_on":
              section.duration = 1e4;
              html += section.value + "\n";
              this.isTyping = message.messageId;
              setTimeout(this.bind(function () {
                this.stopIstyping()
              }), section.duration);
              break;
            default:
              break
          }
          separator = "\n"
        }
        break;
      case"formData":
        html = message.value + "\n";
        document.getElementById("chatAbcdeViewFormSend").setAttribute("action-type", message.type);
        document.getElementById("chatAbcdeViewFormSend").setAttribute("action-value", message.data.name);
        document.getElementById("chatAbcdeViewFormSend").setAttribute("placeholder", message.data.placeholder);
        document.getElementById("chatAbcdeViewFormSend").setAttribute("action-context", message.context);
        break;
      case"typing_on":
        message.duration = 1e4;
        html = message.value + "\n";
        this.isTyping = message.messageId;
        setTimeout(this.bind(function () {
          this.stopIstyping()
        }), message.duration);
        break;
      case"typing_off":
        this.stopIstyping();
        break;
      default:
        break
    }
    html = html.replace(/\<script src\=\'https\:\/\/bot-connector\.orange\.com\/ico\/youtube\.js\'\>\<\/script\>/g, "<svg xmlns='http://www.w3.org/2000/svg' id='icon_youtube' width='20px' viewBox='0 0 1800 1536' style='vertical-align:middle;'><path id='icon_youtube_draw' style='fill:red;' transform='scale(1 -1) translate(0 -1408)' d='M1280 640q0 37 -30 54l-512 320q-31 20 -65 2q-33 -18 -33 -56v-640q0 -38 33 -56q16 -8 31 -8q20 0 34 10l512 320q30 17 30 54zM1792 640q0 -96 -1 -150t-8.5 -136.5t-22.5 -147.5q-16 -73 -69 -123t-124 -58q-222 -25 -671 -25t-671 25q-71 8 -124.5 58t-69.5 123 q-14 65 -21.5 147.5t-8.5 136.5t-1 150t1 150t8.5 136.5t22.5 147.5q16 73 69 123t124 58q222 25 671 25t671 -25q71 -8 124.5 -58t69.5 -123q14 -65 21.5 -147.5t8.5 -136.5t1 -150z' /></svg>");
    html = html.replace(/\<script src\=\'https\:\/\/bot-connector\.orange\.com\/ico\/web\.js\'\>\<\/script\>/g, "<svg xmlns='http://www.w3.org/2000/svg' id='icon_web' width='20px' viewBox='0 0 1536 1536' style='vertical-align:middle;'><path id='icon_web_draw' style='fill:black;' transform='scale(1 -1) translate(0 -1408)' d='M768 1408q209 0 385.5 -103t279.5 -279.5t103 -385.5t-103 -385.5t-279.5 -279.5t-385.5 -103t-385.5 103t-279.5 279.5t-103 385.5t103 385.5t279.5 279.5t385.5 103zM1042 887q-2 -1 -9.5 -9.5t-13.5 -9.5q2 0 4.5 5t5 11t3.5 7q6 7 22 15q14 6 52 12q34 8 51 -11 q-2 2 9.5 13t14.5 12q3 2 15 4.5t15 7.5l2 22q-12 -1 -17.5 7t-6.5 21q0 -2 -6 -8q0 7 -4.5 8t-11.5 -1t-9 -1q-10 3 -15 7.5t-8 16.5t-4 15q-2 5 -9.5 10.5t-9.5 10.5q-1 2 -2.5 5.5t-3 6.5t-4 5.5t-5.5 2.5t-7 -5t-7.5 -10t-4.5 -5q-3 2 -6 1.5t-4.5 -1t-4.5 -3t-5 -3.5 q-3 -2 -8.5 -3t-8.5 -2q15 5 -1 11q-10 4 -16 3q9 4 7.5 12t-8.5 14h5q-1 4 -8.5 8.5t-17.5 8.5t-13 6q-8 5 -34 9.5t-33 0.5q-5 -6 -4.5 -10.5t4 -14t3.5 -12.5q1 -6 -5.5 -13t-6.5 -12q0 -7 14 -15.5t10 -21.5q-3 -8 -16 -16t-16 -12q-5 -8 -1.5 -18.5t10.5 -16.5 q2 -2 1.5 -4t-3.5 -4.5t-5.5 -4t-6.5 -3.5l-3 -2q-11 -5 -20.5 6t-13.5 26q-7 25 -16 30q-23 8 -29 -1q-5 13 -41 26q-25 9 -58 4q6 1 0 15q-7 15 -19 12q3 6 4 17.5t1 13.5q3 13 12 23q1 1 7 8.5t9.5 13.5t0.5 6q35 -4 50 11q5 5 11.5 17t10.5 17q9 6 14 5.5t14.5 -5.5 t14.5 -5q14 -1 15.5 11t-7.5 20q12 -1 3 17q-5 7 -8 9q-12 4 -27 -5q-8 -4 2 -8q-1 1 -9.5 -10.5t-16.5 -17.5t-16 5q-1 1 -5.5 13.5t-9.5 13.5q-8 0 -16 -15q3 8 -11 15t-24 8q19 12 -8 27q-7 4 -20.5 5t-19.5 -4q-5 -7 -5.5 -11.5t5 -8t10.5 -5.5t11.5 -4t8.5 -3 q14 -10 8 -14q-2 -1 -8.5 -3.5t-11.5 -4.5t-6 -4q-3 -4 0 -14t-2 -14q-5 5 -9 17.5t-7 16.5q7 -9 -25 -6l-10 1q-4 0 -16 -2t-20.5 -1t-13.5 8q-4 8 0 20q1 4 4 2q-4 3 -11 9.5t-10 8.5q-46 -15 -94 -41q6 -1 12 1q5 2 13 6.5t10 5.5q34 14 42 7l5 5q14 -16 20 -25 q-7 4 -30 1q-20 -6 -22 -12q7 -12 5 -18q-4 3 -11.5 10t-14.5 11t-15 5q-16 0 -22 -1q-146 -80 -235 -222q7 -7 12 -8q4 -1 5 -9t2.5 -11t11.5 3q9 -8 3 -19q1 1 44 -27q19 -17 21 -21q3 -11 -10 -18q-1 2 -9 9t-9 4q-3 -5 0.5 -18.5t10.5 -12.5q-7 0 -9.5 -16t-2.5 -35.5 t-1 -23.5l2 -1q-3 -12 5.5 -34.5t21.5 -19.5q-13 -3 20 -43q6 -8 8 -9q3 -2 12 -7.5t15 -10t10 -10.5q4 -5 10 -22.5t14 -23.5q-2 -6 9.5 -20t10.5 -23q-1 0 -2.5 -1t-2.5 -1q3 -7 15.5 -14t15.5 -13q1 -3 2 -10t3 -11t8 -2q2 20 -24 62q-15 25 -17 29q-3 5 -5.5 15.5 t-4.5 14.5q2 0 6 -1.5t8.5 -3.5t7.5 -4t2 -3q-3 -7 2 -17.5t12 -18.5t17 -19t12 -13q6 -6 14 -19.5t0 -13.5q9 0 20 -10t17 -20q5 -8 8 -26t5 -24q2 -7 8.5 -13.5t12.5 -9.5l16 -8t13 -7q5 -2 18.5 -10.5t21.5 -11.5q10 -4 16 -4t14.5 2.5t13.5 3.5q15 2 29 -15t21 -21 q36 -19 55 -11q-2 -1 0.5 -7.5t8 -15.5t9 -14.5t5.5 -8.5q5 -6 18 -15t18 -15q6 4 7 9q-3 -8 7 -20t18 -10q14 3 14 32q-31 -15 -49 18q0 1 -2.5 5.5t-4 8.5t-2.5 8.5t0 7.5t5 3q9 0 10 3.5t-2 12.5t-4 13q-1 8 -11 20t-12 15q-5 -9 -16 -8t-16 9q0 -1 -1.5 -5.5t-1.5 -6.5 q-13 0 -15 1q1 3 2.5 17.5t3.5 22.5q1 4 5.5 12t7.5 14.5t4 12.5t-4.5 9.5t-17.5 2.5q-19 -1 -26 -20q-1 -3 -3 -10.5t-5 -11.5t-9 -7q-7 -3 -24 -2t-24 5q-13 8 -22.5 29t-9.5 37q0 10 2.5 26.5t3 25t-5.5 24.5q3 2 9 9.5t10 10.5q2 1 4.5 1.5t4.5 0t4 1.5t3 6q-1 1 -4 3 q-3 3 -4 3q7 -3 28.5 1.5t27.5 -1.5q15 -11 22 2q0 1 -2.5 9.5t-0.5 13.5q5 -27 29 -9q3 -3 15.5 -5t17.5 -5q3 -2 7 -5.5t5.5 -4.5t5 0.5t8.5 6.5q10 -14 12 -24q11 -40 19 -44q7 -3 11 -2t4.5 9.5t0 14t-1.5 12.5l-1 8v18l-1 8q-15 3 -18.5 12t1.5 18.5t15 18.5q1 1 8 3.5 t15.5 6.5t12.5 8q21 19 15 35q7 0 11 9q-1 0 -5 3t-7.5 5t-4.5 2q9 5 2 16q5 3 7.5 11t7.5 10q9 -12 21 -2q7 8 1 16q5 7 20.5 10.5t18.5 9.5q7 -2 8 2t1 12t3 12q4 5 15 9t13 5l17 11q3 4 0 4q18 -2 31 11q10 11 -6 20q3 6 -3 9.5t-15 5.5q3 1 11.5 0.5t10.5 1.5 q15 10 -7 16q-17 5 -43 -12zM879 10q206 36 351 189q-3 3 -12.5 4.5t-12.5 3.5q-18 7 -24 8q1 7 -2.5 13t-8 9t-12.5 8t-11 7q-2 2 -7 6t-7 5.5t-7.5 4.5t-8.5 2t-10 -1l-3 -1q-3 -1 -5.5 -2.5t-5.5 -3t-4 -3t0 -2.5q-21 17 -36 22q-5 1 -11 5.5t-10.5 7t-10 1.5t-11.5 -7 q-5 -5 -6 -15t-2 -13q-7 5 0 17.5t2 18.5q-3 6 -10.5 4.5t-12 -4.5t-11.5 -8.5t-9 -6.5t-8.5 -5.5t-8.5 -7.5q-3 -4 -6 -12t-5 -11q-2 4 -11.5 6.5t-9.5 5.5q2 -10 4 -35t5 -38q7 -31 -12 -48q-27 -25 -29 -40q-4 -22 12 -26q0 -7 -8 -20.5t-7 -21.5q0 -6 2 -16z' /></svg>");
    html = html.replace(/\<script src\=\'https\:\/\/bot-connector\.orange\.com\/ico\/phone\.js\'\>\<\/script\>/g, "<svg xmlns='http://www.w3.org/2000/svg' id='icon_phone' width='20px' viewBox='0 0 1536 1536' style='vertical-align:middle;'><path id='icon_phone_draw' style='fill:black;' transform='scale(1 -1) translate(0 -1408)' d='M1408 296q0 -27 -10 -70.5t-21 -68.5q-21 -50 -122 -106q-94 -51 -186 -51q-27 0 -52.5 3.5t-57.5 12.5t-47.5 14.5t-55.5 20.5t-49 18q-98 35 -175 83q-128 79 -264.5 215.5t-215.5 264.5q-48 77 -83 175q-3 9 -18 49t-20.5 55.5t-14.5 47.5t-12.5 57.5t-3.5 52.5 q0 92 51 186q56 101 106 122q25 11 68.5 21t70.5 10q14 0 21 -3q18 -6 53 -76q11 -19 30 -54t35 -63.5t31 -53.5q3 -4 17.5 -25t21.5 -35.5t7 -28.5q0 -20 -28.5 -50t-62 -55t-62 -53t-28.5 -46q0 -9 5 -22.5t8.5 -20.5t14 -24t11.5 -19q76 -137 174 -235t235 -174 q2 -1 19 -11.5t24 -14t20.5 -8.5t22.5 -5q18 0 46 28.5t53 62t55 62t50 28.5q14 0 28.5 -7t35.5 -21.5t25 -17.5q25 -15 53.5 -31t63.5 -35t54 -30q70 -35 76 -53q3 -7 3 -21z' /></svg>");
    html = html.replace(/\<script src\=\'https\:\/\/bot-connector\.orange\.com\/ico\/locate\.js\'\>\<\/script\>/g, "<svg xmlns='http://www.w3.org/2000/svg' id='icon_locate' width='20px' viewBox='0 0 1536 1536' style='vertical-align:middle;'><path id='icon_locate_draw' d='M12 2C8.13 2 5 5.13 5 9c0 5.25 7 13 7 13s7-7.75 7-13c0-3.87-3.13-7-7-7zm0 9.5c-1.38 0-2.5-1.12-2.5-2.5s1.12-2.5 2.5-2.5 2.5 1.12 2.5 2.5-1.12 2.5-2.5 2.5z'/><path d='M0 0h24v24H0z' fill='none'/></svg>");
    html = html.replace(/\<script src\=\'https\:\/\/bot-connector\.orange\.com\/ico\/map\.js\'\>\<\/script\>/g, "<svg xmlns='http://www.w3.org/2000/svg' id='icon_map' width='20px' viewBox='0 0 1536 1536' style='vertical-align:middle;'><path id='icon_map_draw' d='M12 2C8.13 2 5 5.13 5 9c0 5.25 7 13 7 13s7-7.75 7-13c0-3.87-3.13-7-7-7zm0 9.5c-1.38 0-2.5-1.12-2.5-2.5s1.12-2.5 2.5-2.5 2.5 1.12 2.5 2.5-1.12 2.5-2.5 2.5z'/><path d='M0 0h24v24H0z' fill='none'/></svg>");
    html = html.replace(/\<script src\=\'https\:\/\/bot-connector\.orange\.com\/ico\/calendar\.js\'\>\<\/script\>/g, "<svg xmlns='http://www.w3.org/2000/svg' id='icon_calendar' width='20px' viewBox='0 0 1536 1536' style='vertical-align:middle;'><path id='icon_calendar_draw' d='M9 11H7v2h2v-2zm4 0h-2v2h2v-2zm4 0h-2v2h2v-2zm2-7h-1V2h-2v2H8V2H6v2H5c-1.11 0-1.99.9-1.99 2L3 20c0 1.1.89 2 2 2h14c1.1 0 2-.9 2-2V6c0-1.1-.9-2-2-2zm0 16H5V9h14v11z'/><path fill='none' d='M0 0h24v24H0z'/></svg>");
    var javascript = "";
    while (html.indexOf("<script") !== -1) {
      var code = html.substring(html.indexOf("<script"));
      code = code.substring(code.indexOf(">") + 1);
      code = code.substring(0, code.indexOf("<\/script>"));
      javascript += code + "\n";
      var beginHtml = html.substring(0, html.indexOf("<script"));
      var endHtml = html.substring(html.indexOf("<\/script>") + 9);
      html = beginHtml + endHtml
    }
    var bubble = document.createElement("tr");
    bubble.id = message.messageId;
    bubble.style.display = "flex";
    bubble.innerHTML = "<td style='width:100%;'><div class='bubbleAbcde completedAbcde " + type + "'>" + html + "</div></td>";
    if (document.getElementById(message.messageId) === null) {
      document.getElementById("chatAbcdeViewMessagesList").appendChild(bubble)
    } else {
      var existingMessage = document.getElementById(message.messageId);
      var messageList = existingMessage.parentNode;
      messageList.replaceChild(bubble, existingMessage)
    }
    document.getElementById("chatAbcdeViewMessagesList").scrollTop = document.getElementById("chatAbcdeViewMessagesList").scrollHeight;
    if (javascript.length > 0) {
      var head = document.getElementsByTagName("head")[0];
      var scriptElement = document.createElement("script");
      scriptElement.setAttribute("type", "text/javascript");
      scriptElement.innerText = javascript;
      head.appendChild(scriptElement)
    }
    return true
  };
  this.stopIstyping = function () {
    if (this.isTyping !== undefined && this.isTyping !== null) {
      var el = document.getElementById(this.isTyping);
      if (el !== null) {
        el.remove()
      }
    }
    this.isTyping = null
  }
}

(function () {
  i = 0;
  
  function start(e) {
    i++;
    if (i >= 3) {
      d = document;
      e = d.createElement("div");
      e.id = "chatAbcdeButton";
      e.title = "";
      d.body.appendChild(e);
      e = d.createElement("div");
      e.id = "chatAbcdeView";
      e.innerHTML = '<div id="chatAbcdeViewHeader"><div id="chatAbcdeViewTitle"></div><div id="chatAbcdeViewMin"><i id="chatAbcdeViewMinButton" class="material-icons">minimize</i></div><div id="chatAbcdeViewClose"><i id="chatAbcdeViewCloseButton" class="material-icons">close</i></div></div>' + '<div id="chatAbcdeViewMessages"><table style="width:100%;"><tbody id="chatAbcdeViewMessagesList" style="width:100%;"></tbody></table></div>' + '<div id="chatAbcdeViewForm">' + '<i id="chatAbcdeViewFormMore" class="material-icons">add</i>' + '<input type="file" id="chatAbcdeFileUploader" accept="image/*" />' + '<input type="text" id="chatAbcdeViewFormText" placeholder="Type a message ..." />' + '<i id="chatAbcdeViewFormSend" class="material-icons">send</i>' + "</div>";
      d.body.appendChild(e);
      app = new applicationAbcde;
      app.init()
    }
  }
  
  d = document;
  h = d.getElementsByTagName("head")[0];
  l = d.createElement("link");
  l.setAttribute("rel", "stylesheet");
  l.setAttribute("type", "text/css");
  l.setAttribute("href", "https://fonts.googleapis.com/css?family=Roboto");
  l.onload = start;
  h.appendChild(l);
  l = d.createElement("link");
  l.setAttribute("rel", "stylesheet");
  l.setAttribute("type", "text/css");
  l.setAttribute("href", "https://fonts.googleapis.com/icon?family=Material+Icons");
  l.onload = start;
  h.appendChild(l);
  l = d.createElement("link");
  l.setAttribute("rel", "stylesheet");
  l.setAttribute("type", "text/css");
  l.setAttribute("href", "https://bot-connector.orange.com/css/w.css");
  l.onload = start;
  h.appendChild(l)
})();
!function (e) {
  "use strict";
  
  function t(e, i, a) {
    var o, n = document.createElement("img");
    return n.onerror = function (o) {
      return t.onerror(n, o, e, i, a)
    }, n.onload = function (o) {
      return t.onload(n, o, e, i, a)
    }, "string" == typeof e ? (t.fetchBlob(e, function (i) {
      i ? (e = i, o = t.createObjectURL(e)) : (o = e, a && a.crossOrigin && (n.crossOrigin = a.crossOrigin)), n.src = o
    }, a), n) : t.isInstanceOf("Blob", e) || t.isInstanceOf("File", e) ? (o = n._objectURL = t.createObjectURL(e)) ? (n.src = o, n) : t.readFile(e, function (e) {
      var t = e.target;
      t && t.result ? n.src = t.result : i && i(e)
    }) : void 0
  }
  
  function i(e, i) {
    !e._objectURL || i && i.noRevoke || (t.revokeObjectURL(e._objectURL), delete e._objectURL)
  }
  
  var a = e.createObjectURL && e || e.URL && URL.revokeObjectURL && URL || e.webkitURL && webkitURL;
  t.fetchBlob = function (e, t, i) {
    t()
  }, t.isInstanceOf = function (e, t) {
    return Object.prototype.toString.call(t) === "[object " + e + "]"
  }, t.transform = function (e, t, i, a, o) {
    i(e, o)
  }, t.onerror = function (e, t, a, o, n) {
    i(e, n), o && o.call(e, t)
  }, t.onload = function (e, a, o, n, r) {
    i(e, r), n && t.transform(e, r, n, o, {})
  }, t.createObjectURL = function (e) {
    return !!a && a.createObjectURL(e)
  }, t.revokeObjectURL = function (e) {
    return !!a && a.revokeObjectURL(e)
  }, t.readFile = function (t, i, a) {
    if (e.FileReader) {
      var o = new FileReader;
      if (o.onload = o.onerror = i, a = a || "readAsDataURL", o[a]) return o[a](t), o
    }
    return !1
  }, "function" == typeof define && define.amd ? define(function () {
    return t
  }) : "object" == typeof module && module.exports ? module.exports = t : e.loadImage = t
}("undefined" != typeof window && window || this), function (e) {
  "use strict";
  "function" == typeof define && define.amd ? define(["./load-image"], e) : e("object" == typeof module && module.exports ? require("./load-image") : window.loadImage)
}(function (e) {
  "use strict";
  var t = e.transform;
  e.transform = function (i, a, o, n, r) {
    t.call(e, e.scale(i, a, r), a, o, n, r)
  }, e.transformCoordinates = function () {
  }, e.getTransformedOptions = function (e, t) {
    var i, a, o, n, r = t.aspectRatio;
    if (!r) return t;
    i = {};
    for (a in t) t.hasOwnProperty(a) && (i[a] = t[a]);
    return i.crop = !0, o = e.naturalWidth || e.width, n = e.naturalHeight || e.height, o / n > r ? (i.maxWidth = n * r, i.maxHeight = n) : (i.maxWidth = o, i.maxHeight = o / r), i
  }, e.renderImageToCanvas = function (e, t, i, a, o, n, r, s, l, c) {
    return e.getContext("2d").drawImage(t, i, a, o, n, r, s, l, c), e
  }, e.hasCanvasOption = function (e) {
    return e.canvas || e.crop || !!e.aspectRatio
  }, e.scale = function (t, i, a) {
    function o() {
      var e = Math.max((l || I) / I, (c || v) / v);
      e > 1 && (I *= e, v *= e)
    }
    
    function n() {
      var e = Math.min((r || I) / I, (s || v) / v);
      e < 1 && (I *= e, v *= e)
    }
    
    i = i || {};
    var r, s, l, c, d, u, f, g, p, m, h, S = document.createElement("canvas"),
      b = t.getContext || e.hasCanvasOption(i) && S.getContext, y = t.naturalWidth || t.width,
      x = t.naturalHeight || t.height, I = y, v = x;
    if (b && (f = (i = e.getTransformedOptions(t, i, a)).left || 0, g = i.top || 0, i.sourceWidth ? (d = i.sourceWidth, void 0 !== i.right && void 0 === i.left && (f = y - d - i.right)) : d = y - f - (i.right || 0), i.sourceHeight ? (u = i.sourceHeight, void 0 !== i.bottom && void 0 === i.top && (g = x - u - i.bottom)) : u = x - g - (i.bottom || 0), I = d, v = u), r = i.maxWidth, s = i.maxHeight, l = i.minWidth, c = i.minHeight, b && r && s && i.crop ? (I = r, v = s, (h = d / u - r / s) < 0 ? (u = s * d / r, void 0 === i.top && void 0 === i.bottom && (g = (x - u) / 2)) : h > 0 && (d = r * u / s, void 0 === i.left && void 0 === i.right && (f = (y - d) / 2))) : ((i.contain || i.cover) && (l = r = r || l, c = s = s || c), i.cover ? (n(), o()) : (o(), n())), b) {
      if ((p = i.pixelRatio) > 1 && (S.style.width = I + "px", S.style.height = v + "px", I *= p, v *= p, S.getContext("2d").scale(p, p)), (m = i.downsamplingRatio) > 0 && m < 1 && I < d && v < u) for (; d * m > I;) S.width = d * m, S.height = u * m, e.renderImageToCanvas(S, t, f, g, d, u, 0, 0, S.width, S.height), f = 0, g = 0, d = S.width, u = S.height, (t = document.createElement("canvas")).width = d, t.height = u, e.renderImageToCanvas(t, S, 0, 0, d, u, 0, 0, d, u);
      return S.width = I, S.height = v, e.transformCoordinates(S, i), e.renderImageToCanvas(S, t, f, g, d, u, 0, 0, I, v)
    }
    return t.width = I, t.height = v, t
  }
}), function (e) {
  "use strict";
  "function" == typeof define && define.amd ? define(["./load-image"], e) : e("object" == typeof module && module.exports ? require("./load-image") : window.loadImage)
}(function (e) {
  "use strict";
  var t = "undefined" != typeof Blob && (Blob.prototype.slice || Blob.prototype.webkitSlice || Blob.prototype.mozSlice);
  e.blobSlice = t && function () {
    return (this.slice || this.webkitSlice || this.mozSlice).apply(this, arguments)
  }, e.metaDataParsers = {jpeg: {65505: [], 65517: []}}, e.parseMetaData = function (t, i, a, o) {
    a = a || {}, o = o || {};
    var n = this, r = a.maxMetaDataSize || 262144;
    !!("undefined" != typeof DataView && t && t.size >= 12 && "image/jpeg" === t.type && e.blobSlice) && e.readFile(e.blobSlice.call(t, 0, r), function (t) {
      if (t.target.error) return console.log(t.target.error), void i(o);
      var r, s, l, c, d = t.target.result, u = new DataView(d), f = 2, g = u.byteLength - 4, p = f;
      if (65496 === u.getUint16(0)) {
        for (; f < g && ((r = u.getUint16(f)) >= 65504 && r <= 65519 || 65534 === r);) {
          if (s = u.getUint16(f + 2) + 2, f + s > u.byteLength) {
            console.log("Invalid meta data: Invalid segment size.");
            break
          }
          if (l = e.metaDataParsers.jpeg[r]) for (c = 0; c < l.length; c += 1) l[c].call(n, u, f, s, o, a);
          p = f += s
        }
        !a.disableImageHead && p > 6 && (d.slice ? o.imageHead = d.slice(0, p) : o.imageHead = new Uint8Array(d).subarray(0, p))
      } else console.log("Invalid JPEG file: Missing JPEG marker.");
      i(o)
    }, "readAsArrayBuffer") || i(o)
  }, e.hasMetaOption = function (e) {
    return e && e.meta
  };
  var i = e.transform;
  e.transform = function (t, a, o, n, r) {
    e.hasMetaOption(a) ? e.parseMetaData(n, function (r) {
      i.call(e, t, a, o, n, r)
    }, a, r) : i.apply(e, arguments)
  }
}), function (e) {
  "use strict";
  "function" == typeof define && define.amd ? define(["./load-image", "./load-image-meta"], e) : "object" == typeof module && module.exports ? e(require("./load-image"), require("./load-image-meta")) : e(window.loadImage)
}(function (e) {
  "use strict";
  "undefined" != typeof fetch && "undefined" != typeof Request && (e.fetchBlob = function (t, i, a) {
    if (e.hasMetaOption(a)) return fetch(new Request(t, a)).then(function (e) {
      return e.blob()
    }).then(i).catch(function (e) {
      console.log(e), i()
    });
    i()
  })
}), function (e) {
  "use strict";
  "function" == typeof define && define.amd ? define(["./load-image", "./load-image-scale", "./load-image-meta"], e) : "object" == typeof module && module.exports ? e(require("./load-image"), require("./load-image-scale"), require("./load-image-meta")) : e(window.loadImage)
}(function (e) {
  "use strict";
  var t = e.hasCanvasOption, i = e.hasMetaOption, a = e.transformCoordinates, o = e.getTransformedOptions;
  e.hasCanvasOption = function (i) {
    return !!i.orientation || t.call(e, i)
  }, e.hasMetaOption = function (t) {
    return t && !0 === t.orientation || i.call(e, t)
  }, e.transformCoordinates = function (t, i) {
    a.call(e, t, i);
    var o = t.getContext("2d"), n = t.width, r = t.height, s = t.style.width, l = t.style.height, c = i.orientation;
    if (c && !(c > 8)) switch (c > 4 && (t.width = r, t.height = n, t.style.width = l, t.style.height = s), c) {
      case 2:
        o.translate(n, 0), o.scale(-1, 1);
        break;
      case 3:
        o.translate(n, r), o.rotate(Math.PI);
        break;
      case 4:
        o.translate(0, r), o.scale(1, -1);
        break;
      case 5:
        o.rotate(.5 * Math.PI), o.scale(1, -1);
        break;
      case 6:
        o.rotate(.5 * Math.PI), o.translate(0, -r);
        break;
      case 7:
        o.rotate(.5 * Math.PI), o.translate(n, -r), o.scale(-1, 1);
        break;
      case 8:
        o.rotate(-.5 * Math.PI), o.translate(-n, 0)
    }
  }, e.getTransformedOptions = function (t, i, a) {
    var n, r, s = o.call(e, t, i), l = s.orientation;
    if (!0 === l && a && a.exif && (l = a.exif.get("Orientation")), !l || l > 8 || 1 === l) return s;
    n = {};
    for (r in s) s.hasOwnProperty(r) && (n[r] = s[r]);
    switch (n.orientation = l, l) {
      case 2:
        n.left = s.right, n.right = s.left;
        break;
      case 3:
        n.left = s.right, n.top = s.bottom, n.right = s.left, n.bottom = s.top;
        break;
      case 4:
        n.top = s.bottom, n.bottom = s.top;
        break;
      case 5:
        n.left = s.top, n.top = s.left, n.right = s.bottom, n.bottom = s.right;
        break;
      case 6:
        n.left = s.top, n.top = s.right, n.right = s.bottom, n.bottom = s.left;
        break;
      case 7:
        n.left = s.bottom, n.top = s.right, n.right = s.top, n.bottom = s.left;
        break;
      case 8:
        n.left = s.bottom, n.top = s.left, n.right = s.top, n.bottom = s.right
    }
    return n.orientation > 4 && (n.maxWidth = s.maxHeight, n.maxHeight = s.maxWidth, n.minWidth = s.minHeight, n.minHeight = s.minWidth, n.sourceWidth = s.sourceHeight, n.sourceHeight = s.sourceWidth), n
  }
}), function (e) {
  "use strict";
  "function" == typeof define && define.amd ? define(["./load-image", "./load-image-meta"], e) : "object" == typeof module && module.exports ? e(require("./load-image"), require("./load-image-meta")) : e(window.loadImage)
}(function (e) {
  "use strict";
  e.ExifMap = function () {
    return this
  }, e.ExifMap.prototype.map = {Orientation: 274}, e.ExifMap.prototype.get = function (e) {
    return this[e] || this[this.map[e]]
  }, e.getExifThumbnail = function (t, i, a) {
    if (a && !(i + a > t.byteLength)) return e.createObjectURL(new Blob([t.buffer.slice(i, i + a)]));
    console.log("Invalid Exif data: Invalid thumbnail data.")
  }, e.exifTagTypes = {
    1: {
      getValue: function (e, t) {
        return e.getUint8(t)
      }, size: 1
    }, 2: {
      getValue: function (e, t) {
        return String.fromCharCode(e.getUint8(t))
      }, size: 1, ascii: !0
    }, 3: {
      getValue: function (e, t, i) {
        return e.getUint16(t, i)
      }, size: 2
    }, 4: {
      getValue: function (e, t, i) {
        return e.getUint32(t, i)
      }, size: 4
    }, 5: {
      getValue: function (e, t, i) {
        return e.getUint32(t, i) / e.getUint32(t + 4, i)
      }, size: 8
    }, 9: {
      getValue: function (e, t, i) {
        return e.getInt32(t, i)
      }, size: 4
    }, 10: {
      getValue: function (e, t, i) {
        return e.getInt32(t, i) / e.getInt32(t + 4, i)
      }, size: 8
    }
  }, e.exifTagTypes[7] = e.exifTagTypes[1], e.getExifValue = function (t, i, a, o, n, r) {
    var s, l, c, d, u, f, g = e.exifTagTypes[o];
    if (g) {
      if (s = g.size * n, !((l = s > 4 ? i + t.getUint32(a + 8, r) : a + 8) + s > t.byteLength)) {
        if (1 === n) return g.getValue(t, l, r);
        for (c = [], d = 0; d < n; d += 1) c[d] = g.getValue(t, l + d * g.size, r);
        if (g.ascii) {
          for (u = "", d = 0; d < c.length && "\0" !== (f = c[d]); d += 1) u += f;
          return u
        }
        return c
      }
      console.log("Invalid Exif data: Invalid data offset.")
    } else console.log("Invalid Exif data: Invalid tag type.")
  }, e.parseExifTag = function (t, i, a, o, n) {
    var r = t.getUint16(a, o);
    n.exif[r] = e.getExifValue(t, i, a, t.getUint16(a + 2, o), t.getUint32(a + 4, o), o)
  }, e.parseExifTags = function (e, t, i, a, o) {
    var n, r, s;
    if (i + 6 > e.byteLength) console.log("Invalid Exif data: Invalid directory offset."); else {
      if (n = e.getUint16(i, a), !((r = i + 2 + 12 * n) + 4 > e.byteLength)) {
        for (s = 0; s < n; s += 1) this.parseExifTag(e, t, i + 2 + 12 * s, a, o);
        return e.getUint32(r, a)
      }
      console.log("Invalid Exif data: Invalid directory size.")
    }
  }, e.parseExifData = function (t, i, a, o, n) {
    if (!n.disableExif) {
      var r, s, l, c = i + 10;
      if (1165519206 === t.getUint32(i + 4)) if (c + 8 > t.byteLength) console.log("Invalid Exif data: Invalid segment size."); else if (0 === t.getUint16(i + 8)) {
        switch (t.getUint16(c)) {
          case 18761:
            r = !0;
            break;
          case 19789:
            r = !1;
            break;
          default:
            return void console.log("Invalid Exif data: Invalid byte alignment marker.")
        }
        42 === t.getUint16(c + 2, r) ? (s = t.getUint32(c + 4, r), o.exif = new e.ExifMap, (s = e.parseExifTags(t, c, c + s, r, o)) && !n.disableExifThumbnail && (l = {exif: {}}, s = e.parseExifTags(t, c, c + s, r, l), l.exif[513] && (o.exif.Thumbnail = e.getExifThumbnail(t, c + l.exif[513], l.exif[514]))), o.exif[34665] && !n.disableExifSub && e.parseExifTags(t, c, c + o.exif[34665], r, o), o.exif[34853] && !n.disableExifGps && e.parseExifTags(t, c, c + o.exif[34853], r, o)) : console.log("Invalid Exif data: Missing TIFF marker.")
      } else console.log("Invalid Exif data: Missing byte alignment offset.")
    }
  }, e.metaDataParsers.jpeg[65505].push(e.parseExifData)
}), function (e) {
  "use strict";
  "function" == typeof define && define.amd ? define(["./load-image", "./load-image-exif"], e) : "object" == typeof module && module.exports ? e(require("./load-image"), require("./load-image-exif")) : e(window.loadImage)
}(function (e) {
  "use strict";
  e.ExifMap.prototype.tags = {
    256: "ImageWidth",
    257: "ImageHeight",
    34665: "ExifIFDPointer",
    34853: "GPSInfoIFDPointer",
    40965: "InteroperabilityIFDPointer",
    258: "BitsPerSample",
    259: "Compression",
    262: "PhotometricInterpretation",
    274: "Orientation",
    277: "SamplesPerPixel",
    284: "PlanarConfiguration",
    530: "YCbCrSubSampling",
    531: "YCbCrPositioning",
    282: "XResolution",
    283: "YResolution",
    296: "ResolutionUnit",
    273: "StripOffsets",
    278: "RowsPerStrip",
    279: "StripByteCounts",
    513: "JPEGInterchangeFormat",
    514: "JPEGInterchangeFormatLength",
    301: "TransferFunction",
    318: "WhitePoint",
    319: "PrimaryChromaticities",
    529: "YCbCrCoefficients",
    532: "ReferenceBlackWhite",
    306: "DateTime",
    270: "ImageDescription",
    271: "Make",
    272: "Model",
    305: "Software",
    315: "Artist",
    33432: "Copyright",
    36864: "ExifVersion",
    40960: "FlashpixVersion",
    40961: "ColorSpace",
    40962: "PixelXDimension",
    40963: "PixelYDimension",
    42240: "Gamma",
    37121: "ComponentsConfiguration",
    37122: "CompressedBitsPerPixel",
    37500: "MakerNote",
    37510: "UserComment",
    40964: "RelatedSoundFile",
    36867: "DateTimeOriginal",
    36868: "DateTimeDigitized",
    37520: "SubSecTime",
    37521: "SubSecTimeOriginal",
    37522: "SubSecTimeDigitized",
    33434: "ExposureTime",
    33437: "FNumber",
    34850: "ExposureProgram",
    34852: "SpectralSensitivity",
    34855: "PhotographicSensitivity",
    34856: "OECF",
    34864: "SensitivityType",
    34865: "StandardOutputSensitivity",
    34866: "RecommendedExposureIndex",
    34867: "ISOSpeed",
    34868: "ISOSpeedLatitudeyyy",
    34869: "ISOSpeedLatitudezzz",
    37377: "ShutterSpeedValue",
    37378: "ApertureValue",
    37379: "BrightnessValue",
    37380: "ExposureBias",
    37381: "MaxApertureValue",
    37382: "SubjectDistance",
    37383: "MeteringMode",
    37384: "LightSource",
    37385: "Flash",
    37396: "SubjectArea",
    37386: "FocalLength",
    41483: "FlashEnergy",
    41484: "SpatialFrequencyResponse",
    41486: "FocalPlaneXResolution",
    41487: "FocalPlaneYResolution",
    41488: "FocalPlaneResolutionUnit",
    41492: "SubjectLocation",
    41493: "ExposureIndex",
    41495: "SensingMethod",
    41728: "FileSource",
    41729: "SceneType",
    41730: "CFAPattern",
    41985: "CustomRendered",
    41986: "ExposureMode",
    41987: "WhiteBalance",
    41988: "DigitalZoomRatio",
    41989: "FocalLengthIn35mmFilm",
    41990: "SceneCaptureType",
    41991: "GainControl",
    41992: "Contrast",
    41993: "Saturation",
    41994: "Sharpness",
    41995: "DeviceSettingDescription",
    41996: "SubjectDistanceRange",
    42016: "ImageUniqueID",
    42032: "CameraOwnerName",
    42033: "BodySerialNumber",
    42034: "LensSpecification",
    42035: "LensMake",
    42036: "LensModel",
    42037: "LensSerialNumber",
    0: "GPSVersionID",
    1: "GPSLatitudeRef",
    2: "GPSLatitude",
    3: "GPSLongitudeRef",
    4: "GPSLongitude",
    5: "GPSAltitudeRef",
    6: "GPSAltitude",
    7: "GPSTimeStamp",
    8: "GPSSatellites",
    9: "GPSStatus",
    10: "GPSMeasureMode",
    11: "GPSDOP",
    12: "GPSSpeedRef",
    13: "GPSSpeed",
    14: "GPSTrackRef",
    15: "GPSTrack",
    16: "GPSImgDirectionRef",
    17: "GPSImgDirection",
    18: "GPSMapDatum",
    19: "GPSDestLatitudeRef",
    20: "GPSDestLatitude",
    21: "GPSDestLongitudeRef",
    22: "GPSDestLongitude",
    23: "GPSDestBearingRef",
    24: "GPSDestBearing",
    25: "GPSDestDistanceRef",
    26: "GPSDestDistance",
    27: "GPSProcessingMethod",
    28: "GPSAreaInformation",
    29: "GPSDateStamp",
    30: "GPSDifferential",
    31: "GPSHPositioningError"
  }, e.ExifMap.prototype.stringValues = {
    ExposureProgram: {
      0: "Undefined",
      1: "Manual",
      2: "Normal program",
      3: "Aperture priority",
      4: "Shutter priority",
      5: "Creative program",
      6: "Action program",
      7: "Portrait mode",
      8: "Landscape mode"
    },
    MeteringMode: {
      0: "Unknown",
      1: "Average",
      2: "CenterWeightedAverage",
      3: "Spot",
      4: "MultiSpot",
      5: "Pattern",
      6: "Partial",
      255: "Other"
    },
    LightSource: {
      0: "Unknown",
      1: "Daylight",
      2: "Fluorescent",
      3: "Tungsten (incandescent light)",
      4: "Flash",
      9: "Fine weather",
      10: "Cloudy weather",
      11: "Shade",
      12: "Daylight fluorescent (D 5700 - 7100K)",
      13: "Day white fluorescent (N 4600 - 5400K)",
      14: "Cool white fluorescent (W 3900 - 4500K)",
      15: "White fluorescent (WW 3200 - 3700K)",
      17: "Standard light A",
      18: "Standard light B",
      19: "Standard light C",
      20: "D55",
      21: "D65",
      22: "D75",
      23: "D50",
      24: "ISO studio tungsten",
      255: "Other"
    },
    Flash: {
      0: "Flash did not fire",
      1: "Flash fired",
      5: "Strobe return light not detected",
      7: "Strobe return light detected",
      9: "Flash fired, compulsory flash mode",
      13: "Flash fired, compulsory flash mode, return light not detected",
      15: "Flash fired, compulsory flash mode, return light detected",
      16: "Flash did not fire, compulsory flash mode",
      24: "Flash did not fire, auto mode",
      25: "Flash fired, auto mode",
      29: "Flash fired, auto mode, return light not detected",
      31: "Flash fired, auto mode, return light detected",
      32: "No flash function",
      65: "Flash fired, red-eye reduction mode",
      69: "Flash fired, red-eye reduction mode, return light not detected",
      71: "Flash fired, red-eye reduction mode, return light detected",
      73: "Flash fired, compulsory flash mode, red-eye reduction mode",
      77: "Flash fired, compulsory flash mode, red-eye reduction mode, return light not detected",
      79: "Flash fired, compulsory flash mode, red-eye reduction mode, return light detected",
      89: "Flash fired, auto mode, red-eye reduction mode",
      93: "Flash fired, auto mode, return light not detected, red-eye reduction mode",
      95: "Flash fired, auto mode, return light detected, red-eye reduction mode"
    },
    SensingMethod: {
      1: "Undefined",
      2: "One-chip color area sensor",
      3: "Two-chip color area sensor",
      4: "Three-chip color area sensor",
      5: "Color sequential area sensor",
      7: "Trilinear sensor",
      8: "Color sequential linear sensor"
    },
    SceneCaptureType: {0: "Standard", 1: "Landscape", 2: "Portrait", 3: "Night scene"},
    SceneType: {1: "Directly photographed"},
    CustomRendered: {0: "Normal process", 1: "Custom process"},
    WhiteBalance: {0: "Auto white balance", 1: "Manual white balance"},
    GainControl: {0: "None", 1: "Low gain up", 2: "High gain up", 3: "Low gain down", 4: "High gain down"},
    Contrast: {0: "Normal", 1: "Soft", 2: "Hard"},
    Saturation: {0: "Normal", 1: "Low saturation", 2: "High saturation"},
    Sharpness: {0: "Normal", 1: "Soft", 2: "Hard"},
    SubjectDistanceRange: {0: "Unknown", 1: "Macro", 2: "Close view", 3: "Distant view"},
    FileSource: {3: "DSC"},
    ComponentsConfiguration: {0: "", 1: "Y", 2: "Cb", 3: "Cr", 4: "R", 5: "G", 6: "B"},
    Orientation: {
      1: "top-left",
      2: "top-right",
      3: "bottom-right",
      4: "bottom-left",
      5: "left-top",
      6: "right-top",
      7: "right-bottom",
      8: "left-bottom"
    }
  }, e.ExifMap.prototype.getText = function (e) {
    var t = this.get(e);
    switch (e) {
      case"LightSource":
      case"Flash":
      case"MeteringMode":
      case"ExposureProgram":
      case"SensingMethod":
      case"SceneCaptureType":
      case"SceneType":
      case"CustomRendered":
      case"WhiteBalance":
      case"GainControl":
      case"Contrast":
      case"Saturation":
      case"Sharpness":
      case"SubjectDistanceRange":
      case"FileSource":
      case"Orientation":
        return this.stringValues[e][t];
      case"ExifVersion":
      case"FlashpixVersion":
        if (!t) return;
        return String.fromCharCode(t[0], t[1], t[2], t[3]);
      case"ComponentsConfiguration":
        if (!t) return;
        return this.stringValues[e][t[0]] + this.stringValues[e][t[1]] + this.stringValues[e][t[2]] + this.stringValues[e][t[3]];
      case"GPSVersionID":
        if (!t) return;
        return t[0] + "." + t[1] + "." + t[2] + "." + t[3]
    }
    return String(t)
  }, function (e) {
    var t, i = e.tags, a = e.map;
    for (t in i) i.hasOwnProperty(t) && (a[i[t]] = t)
  }(e.ExifMap.prototype), e.ExifMap.prototype.getAll = function () {
    var e, t, i = {};
    for (e in this) this.hasOwnProperty(e) && (t = this.tags[e]) && (i[t] = this.getText(t));
    return i
  }
}), function (e) {
  "use strict";
  "function" == typeof define && define.amd ? define(["./load-image", "./load-image-meta"], e) : "object" == typeof module && module.exports ? e(require("./load-image"), require("./load-image-meta")) : e(window.loadImage)
}(function (e) {
  "use strict";
  e.IptcMap = function () {
    return this
  }, e.IptcMap.prototype.map = {ObjectName: 5}, e.IptcMap.prototype.get = function (e) {
    return this[e] || this[this.map[e]]
  }, e.parseIptcTags = function (e, t, i, a) {
    for (var o, n, r = t; r < t + i;) 28 === e.getUint8(r) && 2 === e.getUint8(r + 1) && (n = e.getUint8(r + 2)) in a.iptc.tags && (o = function (e, t, i) {
      for (var a = "", o = t; o < t + i; o++) a += String.fromCharCode(e.getUint8(o));
      return a
    }(e, r + 5, e.getInt16(r + 3)), a.iptc.hasOwnProperty(n) ? a.iptc[n] instanceof Array ? a.iptc[n].push(o) : a.iptc[n] = [a.iptc[n], o] : a.iptc[n] = o), r++
  }, e.parseIptcData = function (t, i, a, o, n) {
    if (!n.disableIptc) {
      for (var r = i + a; i + 8 < r;) {
        if (function (e, t) {
          return 943868237 === e.getUint32(t) && 1028 === e.getUint16(t + 4)
        }(t, i)) {
          var s = t.getUint8(i + 7);
          s % 2 != 0 && (s += 1), 0 === s && (s = 4);
          var l = i + 8 + s;
          if (l > r) {
            console.log("Invalid IPTC data: Invalid segment offset.");
            break
          }
          var c = t.getUint16(i + 6 + s);
          if (i + c > r) {
            console.log("Invalid IPTC data: Invalid segment size.");
            break
          }
          return o.iptc = new e.IptcMap, e.parseIptcTags(t, l, c, o)
        }
        i++
      }
      console.log("No IPTC data at this offset - could be XMP")
    }
  }, e.metaDataParsers.jpeg[65517].push(e.parseIptcData)
}), function (e) {
  "use strict";
  "function" == typeof define && define.amd ? define(["./load-image", "./load-image-iptc"], e) : "object" == typeof module && module.exports ? e(require("./load-image"), require("./load-image-iptc")) : e(window.loadImage)
}(function (e) {
  "use strict";
  e.IptcMap.prototype.tags = {
    3: "ObjectType",
    4: "ObjectAttribute",
    5: "ObjectName",
    7: "EditStatus",
    8: "EditorialUpdate",
    10: "Urgency",
    12: "SubjectRef",
    15: "Category",
    20: "SupplCategory",
    22: "FixtureID",
    25: "Keywords",
    26: "ContentLocCode",
    27: "ContentLocName",
    30: "ReleaseDate",
    35: "ReleaseTime",
    37: "ExpirationDate",
    38: "ExpirationTime",
    40: "SpecialInstructions",
    42: "ActionAdvised",
    45: "RefService",
    47: "RefDate",
    50: "RefNumber",
    55: "DateCreated",
    60: "TimeCreated",
    62: "DigitalCreationDate",
    63: "DigitalCreationTime",
    65: "OriginatingProgram",
    70: "ProgramVersion",
    75: "ObjectCycle",
    80: "Byline",
    85: "BylineTitle",
    90: "City",
    92: "Sublocation",
    95: "State",
    100: "CountryCode",
    101: "CountryName",
    103: "OrigTransRef",
    105: "Headline",
    110: "Credit",
    115: "Source",
    116: "CopyrightNotice",
    118: "Contact",
    120: "Caption",
    122: "WriterEditor",
    130: "ImageType",
    131: "ImageOrientation",
    135: "LanguageID"
  }, e.IptcMap.prototype.getText = function (e) {
    var t = this.get(e);
    return String(t)
  }, function (e) {
    var t, i = e.tags, a = e.map || {};
    for (t in i) i.hasOwnProperty(t) && (a[i[t]] = t)
  }(e.IptcMap.prototype), e.IptcMap.prototype.getAll = function () {
    var e, t, i = {};
    for (e in this) this.hasOwnProperty(e) && (t = this.tags[e]) && (i[t] = this.getText(t));
    return i
  }
});
